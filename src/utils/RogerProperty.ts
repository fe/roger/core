// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import dataFactory from "@rdfjs/data-model";
import * as N3 from "n3";

import IRIs from "./IRIs";

export interface RogerPropertyItem {
  filteredQuads: N3.Quad[];
  label: string;
  description: string;
  component: string;
  path: string;
  property: N3.Quad;
  quads: N3.Quad[];
}

export class RogerProperty {
  properties: RogerPropertyItem[] | null = null;

  constructor(store: N3.Store, quads: N3.Quad[]) {
    this.properties = this.getProperties(store, quads);
  }

  private getProperties(
    schemaStore: N3.Store,
    dataQuads: N3.Quad[]
  ): RogerPropertyItem[] | null {
    if (!dataQuads) {
      return null;
    }

    const object = dataQuads?.find((quad) =>
      quad.predicate.equals(dataFactory.namedNode(IRIs.rdf.type))
    )?.object;

    if (!object) {
      return null;
    }

    const subject = schemaStore
      .getSubjects(dataFactory.namedNode(IRIs.sh.targetClass), object, null)
      .pop();

    if (!subject) {
      return null;
    }

    return schemaStore
      .getQuads(subject, null, null, null)
      .filter((q) => q.predicate.value === IRIs.sh.property)
      .map((el) => {
        const quads = schemaStore.getQuads(el.object, null, null, null);
        let path =
          quads.find((quad) =>
            quad.predicate.equals(dataFactory.namedNode(IRIs.sh.path))
          )?.object?.value ?? "";

        const filteredQuads = dataQuads.filter((quad) =>
          quad.predicate.equals(dataFactory.namedNode(path))
        );

        return {
          filteredQuads,
          label:
            quads.find((quad) => quad.predicate.value === IRIs.sh.name)?.object
              ?.value ?? "",
          description:
            quads.find((quad) => quad.predicate.value === IRIs.sh.description)
              ?.object?.value ?? "",
          component:
            quads.find(
              (quad) =>
                quad.predicate.value === "http://datashapes.org/dash#editor"
            )?.object?.value ?? "",
          path,
          property: el,
          quads,
        };
      });
  }
}
