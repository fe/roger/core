// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import type { DatasetCore, Quad } from "@rdfjs/types";
import { getSelectedShape } from "./getters";
import IRIs from "./IRIs";

/**
 * Check if the provided dataQuads are prefillable according to the associated NodeShape.
 *
 * @param schemaStore
 * @param dataQuads
 * @returns
 */
const isPrefillable = (
  schemaStore: DatasetCore,
  dataQuads: Quad[]
): boolean => {
  return getSelectedShape(schemaStore, dataQuads).some(
    (q) => q.predicate.value === IRIs.roger.prefillUrlTemplate
  );
};

export { isPrefillable };
