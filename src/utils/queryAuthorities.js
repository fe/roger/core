// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

/**
 * Get data from authorities. Currently supported:
 * - GND
 * - lobid (based on GND)
 */
import {
  LOBID_GND_BASE_URL,
  LOBID_GND_SEARCH_URL,
  LOBID_RESOURCES_BASE_URL,
} from "./constants";

import axios from "axios";

/**
 * Query Lobid GND API
 * @see https://lobid.org/gnd/api
 * @typedef {Object} queryOptions
 * @property {string} [searchTerm="*"]
 * @property {string} [format="json"]
 * @property {string} [fieldName=""]
 * @property {string} [filterName=""]
 * @property {string} [filterTerm=""]
 * @property {number} [from=0]
 * @property {number} [size=25]
 * @return Promise
 * @deprecated
 */
const queryLobidGND = async ({
  searchTerm = "*",
  format = "json",
  fieldName = "",
  filterName = "",
  filterTerm = "",
  from = 0,
  size = 25,
} = {}) => {
  return axios.get(
    `${LOBID_GND_SEARCH_URL}${
      fieldName ? encodeURI(fieldName) + "%3A" : ""
    }${encodeURI(searchTerm)}${
      filterName && filterTerm
        ? "&filter=" + encodeURI(filterName) + "%3A" + encodeURI(filterTerm)
        : ""
    }${from ? "&from=" + from : ""}${size ? "&size=" + size : ""}${
      format ? "&format=" + format : ""
    }`
  );
};

/** @deprecated */
const getLobidJsonldContext = async (baseUrl) => {
  return axios.get(`${baseUrl}context.jsonld`);
};

/** @deprecated */
const getLobidGNDJsonldContext = async () => {
  return getLobidJsonldContext(LOBID_GND_BASE_URL);
};

/** @deprecated */
const getLobidResourcesJsonldContext = async () => {
  return getLobidJsonldContext(LOBID_RESOURCES_BASE_URL);
};

/** @deprecated */
const getLobidPreview = async (gndId) => {
  // setting headers has no effect
  return axios.get(
    `${LOBID_GND_BASE_URL}${gndId}.preview` /* {
    headers: { Accept: "text/turtle" },
    responseType: "text",
  } */
  );
};

/**
 * Get an item from a uri as provided in the schema turtle with an id provided by the user.
 * @param {String} uriTemplate
 * @param {String} id
 */
const getItem = async (uriTemplate, id) => {
  return axios.get(uriTemplate.split("{}").join(id));
};

export {
  getItem,
  getLobidGNDJsonldContext,
  getLobidPreview,
  getLobidResourcesJsonldContext,
  queryLobidGND,
};
