// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import dataFactory from "@rdfjs/data-model";
import type { DatasetCore, NamedNode, Quad } from "@rdfjs/types";
import IRIs from "./IRIs";

/**
 * Get an array of all possible target paths defined in the schema.
 *
 * @param {*} schemaStore N3Store containing the shacl schema.
 * @returns an array of shacl paths
 */
const getPropertyShapesPaths = (schemaStore: DatasetCore): string[] =>
  schemaStore.getObjects(null, IRIs.sh.path).map((object) => object.value);

/**
 * Get an array of quads of all PropertyShapes in the Schema.
 *
 * @param {} schemaStore N3Store containing the shacl schema.
 * @returns
 */
const getPropertyShapes = (schemaStore: DatasetCore): Quad[][] =>
  schemaStore
    .getSubjects(IRIs.rdf.type, IRIs.sh.PropertyShape)
    .map((subject) => getShape(schemaStore, subject));

/**
 * Get the object value for a certain predicate from an array of quads of the same (property) shape.
 *
 * @param propertyShape
 * @param predicate
 * @returns
 */
const getPropertyValue = (propertyShape: Quad[], predicate: string): string =>
  propertyShape?.find((q) => q.predicate.value === predicate)?.object.value;

/**
 * Get an array of quad arrays with the same NodeShape type.
 *
 * @param schemaStore N3Store containing the shacl schema.
 * @returns an array of quad arrays with the same NodeShape type
 */
const getNodeShapes = (schemaStore: DatasetCore): Quad[][] =>
  schemaStore
    ?.getSubjects(IRIs.rdf.type, IRIs.sh.NodeShape)
    .map((nodeShape) => getShape(schemaStore, nodeShape.value));

/**
 * Get all Quads belonging to a shapeSubject.
 * @param schemaStore N3Store containing the shacl schema.
 * @param shapeSubject
 * @returns The shape object corresponding to the shapeName.
 */
const getShape: Quad[] = (
  schemaStore: DatasetCore,
  shapeSubject: string | NamedNode
) => schemaStore?.getQuads(shapeSubject);

/**
 * Return an Array of PropertyShapes for the nodeShape.
 * @param schemaStore
 * @param nodeShapeQuads
 * @returns
 */
const getShapeProperties: Quad[] = (
  schemaStore: DatasetCore,
  nodeShapeQuads: Quad[]
) => nodeShapeQuads.filter((q) => q.predicate.value === IRIs.sh.property);

/**
 *
 * @param schemaStore
 * @param dataQuads
 * @returns
 */
const getSelectedShape: Quad[] = (
  schemaStore: DatasetCore,
  dataQuads: Quad[]
): Quad[] => {
  // no dataQuads, return empty
  if (!dataQuads.length) return [];
  const subject = schemaStore
    ?.getSubjects(
      dataFactory.namedNode(IRIs.sh.targetClass),
      dataQuads.find((q) => q.predicate.value === IRIs.rdf.type).object
    )
    .pop();
  if (subject) {
    return schemaStore?.getQuads(subject);
  } else {
    // no matching PropertyShape to dataQuads’ type, return empty
    return [];
  }
};

/**
 *
 * @param schemaStore
 * @param dataQuads
 * @returns
 */
const getExportFilename = (
  schemaStore: DatasetCore,
  dataQuads: Quad[]
): string | undefined => {
  // get the quad containing the exportFileNameProperty of the selected NodeShape
  const exportFileNamePropertyShape: Quad = getSelectedShape(
    schemaStore,
    dataQuads
  ).find((q) => q.predicate.value === IRIs.roger.exportFileNameProperty);

  // get the quad containing the path of the property shape from whose data the filename should be selected
  const exportFileNameDataPath: Quad = exportFileNamePropertyShape
    ? getShape(schemaStore, exportFileNamePropertyShape.object).find(
        (q: Quad) => q.predicate.value === IRIs.sh.path
      )
    : undefined;

  // get the object value of the quad in the data store that has the path as predicate
  const filename: string = exportFileNameDataPath
    ? dataQuads.find((q) => q.predicate.equals(exportFileNameDataPath.object))
        .object.value
    : undefined;

  return filename;
};

export {
  getExportFilename,
  getNodeShapes,
  getPropertyShapes,
  getPropertyShapesPaths,
  getPropertyValue,
  getSelectedShape,
  getShape,
  getShapeProperties,
};
