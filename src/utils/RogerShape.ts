// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import * as N3 from "n3";

import IRIs from "./IRIs";

export class RogerShape {
  public shapes: N3.Quad[][];

  constructor(store: N3.Store) {
    this.shapes = this.getShapes(store);
  }

  private getShapes(store: N3.Store): N3.Quad[][] {
    const subjects = store.getSubjects(IRIs.rdf.type, IRIs.sh.NodeShape, null);

    return subjects.map((shape) =>
      store.getQuads(shape.value, null, null, null)
    );
  }
}
