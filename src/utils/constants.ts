// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

export const KEY_CONTEXT = "@context";
export const KEY_ID = "@id";
export const KEY_LANGUAGE = "@language";
export const KEY_TYPE = "@type";
export const KEY_VALUE = "@value";

export const RESULTS_PER_PAGE = [5, 10, 50, 100];

export const GND_BASE_URL = "/gnd/";
export const LOBID_GND_BASE_URL = "/lobid/";
export const LOBID_GND_SEARCH_URL = `${LOBID_GND_BASE_URL}search?q=`;
export const LOBID_RESOURCES_BASE_URL = "/resources/";
export const LOBID_RESOURCES_SEARCH_URL = `${LOBID_RESOURCES_BASE_URL}search?q=`;

// Variable names for SPARQL bindings.
export const S = "subject";
export const P = "predicate";
export const O = "object";
export const T = "text";
export const R = "result";
export const AR = "aggregatedResults";
