// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import * as N3 from "n3";

export class RogerParser {
  private raw: string;

  constructor(data: string) {
    this.raw = data;
  }

  async initialize(): Promise<N3.Store> {
    return this.parse(this.raw);
  }

  private async parse(data: string): Promise<N3.Store> {
    const parser = new N3.Parser();
    const store = new N3.Store();

    return new Promise((resolve, reject) => {
      parser.parse(data, (err, quad) => {
        if (err) {
          reject(err);
        }
        if (quad) {
          store.add(quad);
        }
        return resolve(store);
      });
    });
  }
}
