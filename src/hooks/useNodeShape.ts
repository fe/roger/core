// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import * as N3 from "n3";
import { useMemo } from "react";
import { RogerShape } from "../utils/RogerShape";

export const useNodeShape = (store: N3.Store) => {
  const rogerShapeService = useMemo(() => new RogerShape(store), [store]);

  const shapes = rogerShapeService.shapes;

  return {
    shapes,
  };
};
