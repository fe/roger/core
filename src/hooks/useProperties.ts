// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { useMemo } from "react";
import * as N3 from "n3";
import { RogerProperty } from "../utils/RogerProperty";

export const useProperties = (store: N3.Store, quads: N3.Quad[]) => {
  const rogerPropertyService = useMemo(
    () => new RogerProperty(store, quads),
    [store, quads]
  );

  const properties = rogerPropertyService.properties;

  return {
    properties,
  };
};
