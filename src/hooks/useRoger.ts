// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import * as N3 from "n3";
import { create } from "zustand";

interface RogerExternalEvents {
  onChange: (quads: N3.Quad[]) => Promise<void>;
  onSave: (quads: N3.Quad[]) => Promise<void>;
}

interface RogerStore {
  events: RogerExternalEvents;
  dirty: boolean;
  setDirty: (dirty: boolean) => void;
  setEvents: (events: RogerExternalEvents) => void;
}

const useRogerStateStore = create<RogerStore>((set) => ({
  setEvents: (events) => set((state) => ({ ...state, events })),
  events: {
    onSave: async () => {},
    onChange: async () => {},
  },
  dirty: false,
  setDirty: (dirty) => set((state) => ({ ...state, dirty })),
}));

export const useRoger = () => {
  const { setEvents, events, dirty, setDirty } = useRogerStateStore();

  const handleChange = async (quads: N3.Quad[]) => {
    setDirty(true);
    await events.onChange(quads);
  };

  return {
    setEvents,
    onSave: events.onSave,
    dirty,
    setDirty,
    handleChange,
  };
};
