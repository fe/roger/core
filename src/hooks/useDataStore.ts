// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import * as N3 from "n3";
import { useCallback, useEffect, useState } from "react";

import { RogerParser } from "../utils/RogerParser";

export const useDataStore = (data?: string | Array<N3.Quad> | null) => {
  const [store, setStore] = useState<N3.Store | null>(null);
  const [isLoading, setLoading] = useState(true);

  const initializeStore = useCallback(async () => {
    if (!data) {
      setLoading(false);
      return;
    }

    try {
      setLoading(true);

      let store = new N3.Store();

      if (Array.isArray(data)) {
        store.addQuads(data as N3.Quad[]);
      } else {
        store = await new RogerParser(data).initialize();
      }

      setStore(store);
    } catch (error) {
      console.error(`[Error] - ${error}`);
    } finally {
      setLoading(false);
    }
  }, [data]);

  useEffect(() => {
    initializeStore();
  }, [initializeStore]);

  return {
    store,
    isLoading,
  };
};
