// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

/**
 * @deprecated Use `SparqlConfig` instead
 */
export interface Endpoints {
  endpoint: string;
  update?: string;
}
export interface SparqlConfig {
  queryEndpoint: string;
  updateEndpoint?: string;
  transactionEndpoint?: string;
}
