// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import axios from "axios";

import type { SparqlConfig } from "../types/endpoints";

export class TransactionError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "TransactionError";
    Object.setPrototypeOf(this, TransactionError.prototype);
  }
}

/**
 * Update a resource in a triple store.
 * @param subject The resource’s subject.
 * @param triples The resource’s triples in the "N-Triples" format.
 * @param config A SPARQL-Endpoint configuration.
 */
export async function updateResource(
  subject: string,
  triples: string,
  config: SparqlConfig
) {
  // Start transaction
  const response = await axios.post(config.transactionEndpoint);

  const transactionUrl = response?.headers["location"];

  try {
    // Delete currently stored triples
    const deleteQuery = `DELETE WHERE { <${subject}> ?p ?o }`;
    const deleteResponse = await axios.put(
      `${transactionUrl}?action=UPDATE&update=${encodeURIComponent(
        deleteQuery
      )}`
    );
    console.debug(deleteResponse);
    // Insert new triples
    const insertQuery = `INSERT DATA {${triples}}`;
    const insertResponse = await axios.put(
      `${transactionUrl}?action=UPDATE&update=${encodeURIComponent(
        insertQuery
      )}`
    );
    console.debug(insertResponse);
    // Commit
    const commitResponse = await axios.put(`${transactionUrl}?action=COMMIT`);
    console.debug(commitResponse);
  } catch (error) {
    // Abort the transaction
    const abortResponse = await axios.delete(transactionUrl);
    console.debug(abortResponse);
    throw new TransactionError(`Transaction aborted: ${error}`);
  }
}
