// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { SparqlEndpointFetcher } from "fetch-sparql-endpoint";
import type {
  Quad,
  Quad_Object,
  Quad_Predicate,
  Quad_Subject,
} from "@rdfjs/types";
import { O, P, S } from "../utils/constants";

interface Binding {
  [S]: Quad_Subject;
  [P]: Quad_Predicate;
  [O]: Quad_Object;
}

export async function getBlankNodeQuads(
  endpoint: string,
  subject: string
): Promise<Array<Quad>> {
  const fetcher = new SparqlEndpointFetcher();
  const query = `
            CONSTRUCT WHERE {
              <${subject}> ?Py [?P ?O] .
              <${subject}> ?Px ?Ox .
            }`;
  const bindingsStream = await fetcher.fetchTriples(endpoint, query);
  return new Promise((resolve, reject) => {
    const quads: Quad[] = [];
    bindingsStream
      .on("error", (err: Error) => reject(err))
      .on("data", (binding: Binding) => {
        quads.push(binding);
      })
      .on("finish", () => {
        resolve(quads);
      });
  });
}
