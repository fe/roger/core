// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { SparqlEndpointFetcher } from "fetch-sparql-endpoint";
import type { Quad_Subject } from "@rdfjs/types";
import { S } from "../utils/constants";

/** Binding to the Subject of a Quad. */
interface SubjectBinding {
  [S]: Quad_Subject;
}

export async function getSubjects(
  endpoint: string,
  targetClass: string
): Promise<Array<Quad_Subject>> {
  const fetcher = new SparqlEndpointFetcher();
  const query = `SELECT DISTINCT ?${S} WHERE {?${S} a <${targetClass}>}`;
  const bindingsStream = await fetcher.fetchBindings(endpoint, query);
  const subjects: Quad_Subject[] = [];

  return new Promise((resolve, reject) => {
    bindingsStream
      .on("error", (err) => reject(err))
      .on("data", (binding: SubjectBinding) => {
        subjects.push(binding[S]);
      })
      .on("finish", () => {
        resolve(subjects);
      });
  });
}
