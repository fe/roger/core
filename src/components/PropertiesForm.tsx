// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import AddBoxIcon from "@mui/icons-material/AddBox";
import DeleteIcon from "@mui/icons-material/Delete";
import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  IconButton,
  Stack,
} from "@mui/material";
import dataFactory from "@rdfjs/data-model";
import * as N3 from "n3";
import { Quad } from "n3";
import React from "react";

import { useProperties } from "../hooks";
import { componentMapping } from "./mapping";
import { SparqlConfig } from "../types/endpoints";
import IRIs from "../utils/IRIs";
import { RogerPropertyItem } from "../utils/RogerProperty";

interface PropertiesFormProps {
  store: N3.Store;
  quads: N3.Quad[];
  onQuadsUpdate: (data: N3.Quad[]) => void;
  endpoints?: SparqlConfig;
  rootQuads: N3.Quad[];
}

export const PropertiesForm: React.FC<PropertiesFormProps> = ({
  quads,
  store,
  onQuadsUpdate,
  endpoints,
  rootQuads,
}) => {
  const { properties } = useProperties(store, quads);

  const handleAdd = async (property: RogerPropertyItem) => {
    const shaclClass = property.quads.find(
      (el) => el.predicate.value === IRIs.sh.class
    );

    let updated: any[] = [];

    if (shaclClass) {
      const blankNode = dataFactory.blankNode();
      const containerNode = dataFactory.quad(
        quads[0].subject,
        dataFactory.namedNode(property.path),
        blankNode
      );
      const containingNode = dataFactory.quad(
        blankNode,
        dataFactory.namedNode(IRIs.rdf.type),
        shaclClass.object
      );
      updated = [...quads, containerNode, containingNode];
    } else {
      const newQuad = dataFactory.quad(
        quads[0].subject,
        dataFactory.namedNode(property.path),
        dataFactory.literal("")
      );

      // find the index of the last Quad with the predicate value of path
      const index = quads.findLastIndex(
        (q) => q.predicate.value === property.path
      );
      if (index != -1) {
        // Quad found. Insert newQuad after last Quad of the same path.
        updated = [
          ...quads.slice(0, index + 1),
          newQuad,
          ...quads.slice(index + 1),
        ];
      } else {
        // No Quad found. Append newQuad to the end of dataQuads.
        updated = [...quads, newQuad];
      }
    }

    onQuadsUpdate(updated);
  };

  const handleRemove = async (quad: N3.Quad) => {
    const newQuads = quads.filter(
      (q) => q !== quad && q.subject.value !== quad.object.value
    );

    onQuadsUpdate(newQuads);
  };

  /** Handle input data change. */
  const handleChange = async (value: any, quad: N3.Quad) => {
    const newQuad = dataFactory.fromQuad(quad);
    const propertyShapeSubject = store
      .getSubjects(IRIs.sh.path, quad.predicate.value, null)
      .pop();

    if (!propertyShapeSubject) {
      return null;
    }

    const propertyShape = store.getQuads(
      propertyShapeSubject,
      null,
      null,
      null
    );
    const datatype = propertyShape.find(
      (q) => q.predicate.value === IRIs.sh.datatype
    );

    const object = datatype
      ? dataFactory.literal(value)
      : dataFactory.namedNode(value);

    const previousObjectValue = quad.object.value;
    newQuad.object = object;

    const newDataQuads: Quad[] = quads.reduce((newQuads: Quad[], q: Quad) => {
      if (q === quad) {
        newQuads.push(newQuad as Quad);
      } else if (q.subject.value !== previousObjectValue) {
        newQuads.push(q);
      }
      return newQuads;
    }, []);

    onQuadsUpdate(newDataQuads);
  };

  if (!properties) {
    return null;
  }

  const filterQuads = (quad: N3.Quad) => {
    const rootQuadFilter = rootQuads.filter((rootQuad) =>
      rootQuad.subject.equals(quad.object)
    );

    const quadFilter = quads.filter((el) => el.subject.equals(quad.object));

    return rootQuadFilter.length ? rootQuadFilter : quadFilter;
  };

  return (
    <Stack spacing={3} width="100%">
      {properties.map((property, index) => (
        <Card key={index}>
          <CardHeader title={property.label} subheader={property.description} />
          <CardContent
            sx={{
              paddingTop: 0,
              paddingBottom: 0,
            }}
          >
            <Stack spacing={2}>
              {property.filteredQuads.map((quad, quadIndex) => (
                <Stack
                  direction="row"
                  spacing={2}
                  key={`${quadIndex}_${index}`}
                >
                  {React.createElement(
                    componentMapping[
                      property.component as keyof typeof componentMapping
                    ],
                    {
                      rootQuads: rootQuads,
                      data:
                        quad.object.termType === "BlankNode"
                          ? filterQuads(quad)
                          : quad.object.value,
                      onChange: {
                        handleAdd,
                        handleRemove,
                        handleChange,
                        handleQuadsUpdate: (data: N3.Quad[]) =>
                          handleChange(data, quad),
                      },
                      path: property.path,
                      quad: quad,
                      schema: store,
                      sparqlEndpoint: IRIs.roger.sparqlEndpoint,
                      sparqlStatement: IRIs.roger.sparqlStatement,
                      property: property.property,
                      endpoints: endpoints,
                      label: property.label,
                    }
                  )}
                  <Stack paddingY={1}>
                    <IconButton onClick={() => handleRemove(quad)}>
                      <DeleteIcon />
                    </IconButton>
                  </Stack>
                </Stack>
              ))}
            </Stack>
          </CardContent>
          <CardActions>
            <Box mx="auto">
              <IconButton onClick={() => handleAdd(property)}>
                <AddBoxIcon />
              </IconButton>
            </Box>
          </CardActions>
        </Card>
      ))}
    </Stack>
  );
};
