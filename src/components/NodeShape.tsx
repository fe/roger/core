// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import dataFactory from "@rdfjs/data-model";
import * as N3 from "n3";
import React, { useEffect, useMemo, useState } from "react";
import { v4 as uuidv4 } from "uuid";

import { useNodeShape } from "../hooks/useNodeShape";
import IRIs from "../utils/IRIs";

const SELECT_LABEL = "Shape";
const SELECT_LABEL_ID = "select-shape-label";

interface NodeShapeProps {
  data: N3.Store;
  selectedShape: {
    label: string;
    value: N3.Quad[];
  } | null;
  setSelectedShape: (
    value: {
      label: string;
      value: N3.Quad[];
    } | null
  ) => void;
}

export const NodeShape: React.FC<NodeShapeProps> = ({
  data,
  selectedShape,
  setSelectedShape,
}) => {
  const { shapes } = useNodeShape(data);

  // Filter the shapes to include only those that do not have formNode == "false"
  const filteredShapes = useMemo(
    () =>
      shapes
        .filter(
          (shape) =>
            !shape.some(
              (q) =>
                q.predicate.equals(
                  dataFactory.namedNode(IRIs.roger.formNode)
                ) &&
                q.object.equals(
                  dataFactory.literal(
                    "false",
                    dataFactory.namedNode(IRIs.xsd.boolean)
                  )
                )
            )
        )
        .map((el) => ({
          value: el,
          // look for shacl name; fall back to rdfs label if not found
          label:
            (
              el.find((q) => q.predicate.value === IRIs.sh.name) ||
              el.find((q) => q.predicate.value === IRIs.rdfs.label)
            )?.object.value || el?.[0]?.subject.value,
        })),
    [shapes]
  );

  const handleShapeChange = (label: string) => {
    const matchedShape = filteredShapes.find((el) => el.label === label);
    if (!matchedShape) {
      return;
    }
    setSelectedShape(matchedShape);
  };

  useEffect(() => {
    if (filteredShapes.length) {
      handleShapeChange(filteredShapes?.[0]?.label);
    }
  }, [filteredShapes]);

  return (
    <>
      {filteredShapes.length > 0 && (
        <FormControl fullWidth variant="outlined">
          <InputLabel id={SELECT_LABEL_ID}>{SELECT_LABEL}</InputLabel>
          <Select
            label={SELECT_LABEL}
            labelId={SELECT_LABEL_ID}
            value={selectedShape ? selectedShape.label : ""}
            onChange={(event) => handleShapeChange(event.target.value)}
            variant="outlined"
          >
            {filteredShapes.map((shape) => (
              <MenuItem key={shape.label} value={shape.label}>
                {shape.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      )}
    </>
  );
};
