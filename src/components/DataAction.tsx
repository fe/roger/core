// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { Button, ButtonGroup } from "@mui/material";
import type { DatasetCore } from "@rdfjs/types";
import * as N3 from "n3";
import React, { useState } from "react";

import { useRoger } from "../hooks";
import { getExportFilename } from "../utils";
import IRIs from "../utils/IRIs";

interface DataActionProps {
  setDataQuads: (data: DataActionProps["dataQuads"]) => void;
  dataQuads: N3.Quad[];
  schema: DatasetCore;
}

const DataAction: React.FC<DataActionProps> = ({
  setDataQuads,
  dataQuads,
  schema,
}) => {
  const [isSaving, setSaving] = useState(false);
  const { onSave, dirty } = useRoger();

  /**
   * Clear the dataQuads from input data but retain the quad that has the
   * rdf:type predicate. (Necessary for keeping track on the selectedShape and
   * the Subject of all data quads.)
   */
  const handleClickClear = () => {
    let matchedQuad = dataQuads.find(
      (q) => q.predicate.value === IRIs.rdf.type
    );
    if (matchedQuad) {
      setDataQuads([matchedQuad]);
    }
  };

  /** Write dataQuads into a blob and induce a file download in the browser. */
  const handleClickExport = async () => {
    const writer = new N3.Writer();
    writer.addQuads(dataQuads);
    const data = await new Promise<string>((resolve, reject) => {
      writer.end((error, result) => {
        if (error) return reject(error);
        if (result) return resolve(result);
      });
    });
    const blob = new Blob([data]);
    const downloadUrl = URL.createObjectURL(blob);
    let link = document.createElement("a");
    const filename = getExportFilename(schema, dataQuads);
    link.download = `${filename ? filename : "export"}.ttl`;
    link.href = downloadUrl;
    link.click();
    window.URL.revokeObjectURL(downloadUrl);
  };

  const handleSave = async () => {
    setSaving(true);
    await onSave(dataQuads);
    setSaving(false);
  };

  return (
    <ButtonGroup>
      <Button onClick={handleClickClear}>Clear</Button>
      <Button onClick={handleClickExport}>Export</Button>
      <Button disabled={isSaving || !dirty} onClick={handleSave}>
        Save
      </Button>
    </ButtonGroup>
  );
};

export default DataAction;
