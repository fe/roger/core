// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import NiceModal from "@ebay/nice-modal-react";
import { Typography } from "@mui/material";
import dataFactory from "@rdfjs/data-model";
import N3 from "n3";
import React, { useEffect } from "react";

import { useDataStore, useRoger } from "../hooks";
import { Layout } from "./Layout";
import { SparqlConfig } from "../types/endpoints";

interface RogerProps {
  data?: string | Array<N3.Quad>;
  schema: string;
  onSave?: (quads: N3.Quad[]) => Promise<void>;
  onChange?: (quads: N3.Quad[]) => void;
  endpoints?: SparqlConfig;
}

const Roger: React.FC<RogerProps> = ({
  data,
  schema,
  onSave,
  endpoints,
  onChange,
}) => {
  const { store: schemaStore, isLoading: isSchemaStoreLoading } =
    useDataStore(schema);

  const { setEvents } = useRoger();

  function flattenQuads(quads: N3.Quad[]): N3.Quad[] {
    const flattenedQuads: N3.Quad[] = [];
    const quadSet = new Set<string>();

    const processQuad = (quad: N3.Quad) => {
      const quadString = `${quad.subject.value}|${quad.predicate.value}|${quad.object.value}`;

      if (quadSet.has(quadString)) {
        return;
      }

      quadSet.add(quadString);

      if (Array.isArray(quad.object.value)) {
        const nestedQuads = quad.object.value as N3.Quad[];
        const firstNestedQuad = nestedQuads[0];
        const subject =
          quad.subject.termType === "NamedNode"
            ? dataFactory.namedNode(quad.subject.value)
            : dataFactory.blankNode(quad.subject.value);
        const linkingQuad = dataFactory.quad(
          subject,
          dataFactory.namedNode(quad.predicate.value),
          dataFactory.blankNode(firstNestedQuad.subject.value)
        );
        processQuad(linkingQuad);
        nestedQuads.forEach((nestedQuad) => processQuad(nestedQuad));
      } else {
        flattenedQuads.push(quad);
      }
    };

    quads.forEach((quad) => processQuad(quad));

    return flattenedQuads;
  }

  const handleSave = async (quads: N3.Quad[]) => {
    onSave?.(quads);
  };

  const handleChange = async (quads: N3.Quad[]) => {
    onChange?.(quads);
  };

  useEffect(() => {
    setEvents({
      onSave: handleSave,
      onChange: handleChange,
    });
  }, []);

  if (isSchemaStoreLoading) {
    return <Typography>Loading...</Typography>;
  }

  if (!schemaStore) {
    return <Typography>Something went wrong!!</Typography>;
  }

  return (
    <NiceModal.Provider>
      <Layout data={data} schema={schemaStore} endpoints={endpoints} />
    </NiceModal.Provider>
  );
};

export default Roger;
