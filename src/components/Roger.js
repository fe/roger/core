// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React, { useEffect } from "react";
import { Button, ButtonGroup, Container, Stack } from "@mui/material";
import { v4 as uuidv4 } from "uuid";

import dataFactory from "@rdfjs/data-model";
import * as N3 from "n3";
import { endPromise, parsePromise } from "../promises/N3Promises";
import { isPrefillable } from "../utils/checks";
import {
  getExportFilename,
  getNodeShapes,
  getPropertyShapesPaths,
  getSelectedShape,
  getShape,
} from "../utils";
import IRIs from "../utils/IRIs";
import { getItem } from "../utils/queryAuthorities";
import SelectShape from "./SelectShape";
import ShapeForm from "./ShapeForm";
import PrefillInput from "./PrefillInput";

/**
 * This is the top level of the ROGER component.
 *
 * Describe its API here and move subcomponents and implementation details to other files.
 * STOP and think about it before changing the API.
 *
 * data: Quad[]
 * schema: N3Store/DatasetCore
 */
export default function Roger({
  data,
  onSave: handleSave,
  schema: schemaStore,
  useChanged: [changed, setChanged] = [],
} = {}) {
  /** An array of Quads to keep track of changes on input data. */
  const [dataQuads, setDataQuads] = React.useState([]);

  /** Fill dataQuads state with data from props. */
  useEffect(() => {
    if (data) setDataQuads(data);
  }, [data]);

  // **************
  // event handlers
  // **************

  /**
   * Clear the dataQuads from input data but retain the quad that has the
   * rdf:type predicate. (Necessary for keeping track on the selectedShape and
   * the Subject of all data quads.)
   */
  const handleClickClear = () => {
    setDataQuads([dataQuads.find((q) => q.predicate.value === IRIs.rdf.type)]);
  };

  /**
   * Create a quad with a random ID if no data (with ID) is given
   * or parsing of the data failed for some reason. Add/update the data type
   * of the selected NodeShape.
   */
  const handleSelectShape = (shape) => {
    const targetClass = schemaStore
      ?.getObjects(shape, IRIs.sh.targetClass)
      .pop().value;

    const index = dataQuads.findIndex(
      (q) => q.predicate.value === IRIs.rdf.type
    );

    if (index != -1) {
      // create a copy of the old quad
      const newQuad = dataFactory.fromQuad(dataQuads[index]);
      // set new target class in the quad
      newQuad.object.value = targetClass;
      // create a copy of the dataset
      const newDataQuads = [...dataQuads];
      // replace old quad with new one
      newDataQuads[index] = newQuad;
      // add quad to the updated store
      setDataQuads(newDataQuads);
    } else {
      // create a new quad with a random id and the selected shape's target class
      const nQuad = dataFactory.quad(
        dataFactory.namedNode(
          `https://www.sub.uni-goettingen.de/roger/schema#${uuidv4()}`
        ),
        dataFactory.namedNode(IRIs.rdf.type),
        dataFactory.namedNode(targetClass)
      );
      setDataQuads([nQuad]);
    }
  };

  /** Write dataQuads into a blob and induce a file download in the browser. */
  const handleClickExport = async () => {
    const writer = new N3.Writer();
    writer.addQuads(dataQuads);
    const data = await endPromise(writer);
    const blob = new Blob([data]);
    const downloadUrl = URL.createObjectURL(blob);
    let link = document.createElement("a");
    const filename = getExportFilename(schemaStore, dataQuads);
    link.download = `${filename ? filename : "export"}.ttl`;
    link.href = downloadUrl;
    link.click();
    window.URL.revokeObjectURL(downloadUrl);
  };

  /** Handle input data change. */
  const handleChange = (value, quad) => {
    const newQuad = dataFactory.fromQuad(quad);
    const propertyShapeSubject = schemaStore
      .getSubjects(IRIs.sh.path, quad.predicate.value)
      .pop();
    const propertyShape = getShape(schemaStore, propertyShapeSubject);
    const datatype = propertyShape.find(
      (q) => q.predicate.value === IRIs.sh.datatype
    );
    const object = datatype
      ? dataFactory.literal(value)
      : dataFactory.namedNode(value);
    newQuad.object = object;
    const index = dataQuads.indexOf(quad);
    // create a copy of the dataset
    const newDataQuads = [...dataQuads];
    // replace old quad with new one
    newDataQuads[index] = newQuad;
    setDataQuads(newDataQuads);
    if (!changed && setChanged) setChanged(true);
  };

  /**
   * Prepare data to be handed up to the parent component and, ultimately, call
   * the save-handler handed down in the onSave prop.
   */
  const handleClickSave = () => {
    handleSave(dataQuads);
    setChanged(false);
  };

  /** Add new Quads to the dataQuads. */
  const handleAdd = (path, data, shape) => {
    // SHACL class is defined in the shape?
    if (shape.find((q) => q.predicate.value === IRIs.sh.class)) {
      // Create a blankNode Quad of that class and link it with a Quad of the defined path.
      const newBlankNode = dataFactory.blankNode();
      const containerQuad = dataFactory.quad(
        data[0].subject,
        dataFactory.namedNode(path),
        newBlankNode
      );
      const containingQuad = dataFactory.quad(
        newBlankNode,
        dataFactory.namedNode(IRIs.rdf.type),
        shape.find((q) => q.predicate.value === IRIs.sh.class).object
      );
      // append new Quads to the end of dataQuads
      const newQuads = [...dataQuads, containerQuad, containingQuad];
      setDataQuads(newQuads);
    } else {
      // SHACL class not defined. Create Quad with subject given by data.
      const newQuad = dataFactory.quad(
        data[0].subject,
        dataFactory.namedNode(path),
        dataFactory.literal("")
      );
      // find the index of the last Quad with the predicate value of path
      const index = data.findLastIndex((q) => q.predicate.value === path);
      let newQuads = [];
      if (index != -1) {
        // Quad found. Insert newQuad after last Quad of the same path.
        newQuads = [
          ...dataQuads.slice(0, index + 1),
          newQuad,
          ...dataQuads.slice(index + 1),
        ];
      } else {
        // No Quad found. Append newQuad to the end of dataQuads.
        newQuads = [...dataQuads, newQuad];
      }
      setDataQuads(newQuads);
    }
    if (!changed && setChanged) setChanged(true);
  };

  /** Remove a quad from the dataQuads. */
  const handleRemove = (quad) => {
    // TODO: check removal of blank node quads (nested forms)
    const index = dataQuads.indexOf(quad);
    const newQuads = [
      ...dataQuads.slice(0, index),
      ...dataQuads.slice(index + 1),
    ];
    setDataQuads(newQuads);
    if (!changed && setChanged) setChanged(true);
  };

  /** Fetch data, parse it, and add it to the dataQuads. */
  const handleSelectRemoteInstance = async (iri) => {
    const id = iri.split("/").pop();
    const urlTemplate = getSelectedShape(schemaStore, dataQuads).find(
      (q) => q.predicate.value === IRIs.roger.prefillUrlTemplate
    ).object.value;
    let response;
    try {
      response = await getItem(urlTemplate, id);
    } catch (error) {
      console.log(error.message); // TODO: handle messaging...
      return;
    }

    const data = response.data;
    const parser = new N3.Parser();
    const store = new N3.Store();
    try {
      await parsePromise(parser, data, store);
    } catch (error) {
      console.log(error.message); // TODO: handle messaging...
      return;
    }

    const paths = getPropertyShapesPaths(schemaStore);
    const filteredQuads = [...dataQuads];
    paths?.forEach((path) =>
      filteredQuads.push(
        ...store.getQuads(iri).filter((q) => q.predicate.id === path)
      )
    );
    setDataQuads(filteredQuads);
    if (!changed && setChanged) setChanged(true);
  };

  return (
    <Container>
      {schemaStore?.size ? (
        <Stack spacing={2}>
          <Stack direction="row" spacing={2}>
            <SelectShape
              shapes={getNodeShapes(schemaStore)}
              selectedShape={getSelectedShape(schemaStore, dataQuads)}
              onSelectShape={handleSelectShape}
            />
            <PrefillInput
              disabled={!isPrefillable(schemaStore, dataQuads)}
              onSelectRemoteInstance={handleSelectRemoteInstance}
            />
          </Stack>
          {getSelectedShape(schemaStore, dataQuads).length ? (
            <>
              <ShapeForm
                schema={schemaStore}
                data={dataQuads}
                onChange={{ handleAdd, handleRemove, handleChange }}
              ></ShapeForm>
              <ButtonGroup>
                <Button onClick={handleClickClear}>Clear</Button>
                <Button onClick={handleClickExport}>Export</Button>
                <Button
                  disabled={!(handleSave && changed)}
                  onClick={handleClickSave}
                >
                  Save
                </Button>
              </ButtonGroup>
            </>
          ) : (
            "Select a Shape."
          )}
        </Stack>
      ) : (
        "Schema empty or corrupt."
      )}
    </Container>
  );
}
