// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { Container, Stack, Typography } from "@mui/material";
import dataFactory from "@rdfjs/data-model";
import axios from "axios";
import * as N3 from "n3";
import React, { useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid";

import DataAction from "./DataAction";
import { NodeShape } from "./NodeShape";
import PrefillInput from "./PrefillInput";
import { PropertiesForm } from "./PropertiesForm";
import { useRoger, useDataStore } from "../hooks";
import { SparqlConfig } from "../types/endpoints";
import IRIs from "../utils/IRIs";
import { RogerParser } from "../utils/RogerParser";

interface LayoutProps {
  data?: string | N3.Quad[] | null;
  schema: N3.Store;
  endpoints?: SparqlConfig;
}

export const Layout: React.FC<LayoutProps> = ({
  data: initialDataSchema,
  schema,
  endpoints,
}) => {
  const [dataSchema, setDataSchema] = useState<
    N3.Quad[] | null | string | undefined
  >(initialDataSchema);

  const [isInitialized, setInitialized] = useState<boolean>(false);
  const { handleChange: handleRogerChange } = useRoger();

  useEffect(() => {
    setInitialized(!initialDataSchema);
    setDataSchema(initialDataSchema);
  }, [initialDataSchema]);

  const { store: data } = useDataStore(dataSchema);

  useEffect(() => {
    if (!isInitialized && data) {
      setInitialized(true);
      setQuads(data.getQuads(null, null, null, null));
    }
  }, [data, isInitialized]);

  const [quads, setQuads] = useState<N3.Quad[]>([]);

  const isStoreLoadFailed = !(data?.size || schema.size);
  const [selectedShape, setSelectedShape] = useState<{
    label: string;
    value: N3.Quad[];
  } | null>(null);

  /** Fetch data, parse it, and add it to the dataQuads. */
  const handleSelectRemoteInstance = async (iri: string) => {
    const id = iri.split("/").pop();
    if (!selectedShape) {
      return true;
    }
    let subject = selectedShape.value[0]?.subject?.value;
    const urlTemplate = schema
      .getQuads(subject, null, null, null)
      .find((q) => q.predicate.value === IRIs.roger.prefillUrlTemplate)
      ?.object?.value;

    if (!urlTemplate) {
      return;
    }

    let response;
    try {
      response = await axios.get<string>(urlTemplate.split("{}").join(id));
      setDataSchema(response.data);

      if (subject) {
        const store = await new RogerParser(response.data).initialize();
        handleSelectShape(subject, store.getQuads(null, null, null, null));
      }
    } catch (error) {
      return;
    }
  };

  const handleSelectShape = (subject: N3.OTerm, quads: N3.Quad[]) => {
    const targetClass = schema
      .getObjects(subject, IRIs.sh.targetClass, null)
      .pop()?.value;

    if (!targetClass) {
      return;
    }

    const index = quads.findIndex((q) => q.predicate.value === IRIs.rdf.type);

    if (index > -1) {
      const newQuad = dataFactory.fromQuad(quads[index]);

      newQuad.object.value = targetClass;

      const newDataQuads = [...quads];

      newDataQuads[index] = newQuad as N3.Quad;

      setQuads(newDataQuads);
    } else {
      const nQuad = dataFactory.quad(
        dataFactory.namedNode(
          `https://www.sub.uni-goettingen.de/roger/schema#${uuidv4()}`
        ),
        dataFactory.namedNode(IRIs.rdf.type),
        dataFactory.namedNode(targetClass)
      );
      setQuads([nQuad as N3.Quad]);
    }
  };

  const handleShapeSelect = (value: any) => {
    setSelectedShape(value);
    let subject = value?.value[0]?.subject?.value;

    if (subject) {
      handleSelectShape(subject, quads);
    }
  };

  const handleQuadsUpdate = async (quads: N3.Quad[]) => {
    setQuads(quads);

    await handleRogerChange(quads);
  };

  if (isStoreLoadFailed) {
    return <Typography>Schema empty or corrupt.</Typography>;
  }

  return (
    <Container>
      <Stack spacing={2}>
        <Stack direction="row" spacing={2}>
          <NodeShape
            data={schema}
            selectedShape={selectedShape}
            setSelectedShape={handleShapeSelect}
          />
          <PrefillInput
            schema={schema}
            selectedShape={selectedShape}
            onSelectRemoteInstance={handleSelectRemoteInstance}
          />
        </Stack>

        {!!quads.length && (
          <>
            <PropertiesForm
              store={schema}
              quads={quads}
              onQuadsUpdate={handleQuadsUpdate}
              rootQuads={quads}
              endpoints={endpoints}
            />
            <DataAction
              dataQuads={quads}
              setDataQuads={setQuads}
              schema={schema}
            />
          </>
        )}
      </Stack>
    </Container>
  );
};
