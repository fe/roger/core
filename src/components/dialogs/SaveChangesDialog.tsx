// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React from "react";

import {
  Button,
  ButtonGroup,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

interface SaveChangesDialogProps {
  currentSubject: string;
  open: boolean;
  onDiscard: () => void;
  onSave: () => void;
  onCancel: () => void;
}

export default function SaveChangesDialog({
  currentSubject,
  open,
  onDiscard: handleDiscard,
  onSave: handleSave,
  onCancel: handleCancel,
}: SaveChangesDialogProps) {
  return (
    <Dialog
      open={open}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{`Save changes to ${currentSubject}?`}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          You have changes in this tab that will be lost if you proceed without
          saving.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <ButtonGroup>
          <Button onClick={handleDiscard}>Discard changes</Button>
          <Button onClick={handleCancel}>Cancel</Button>
          <Button onClick={handleSave} variant="contained">
            Save changes
          </Button>
        </ButtonGroup>
      </DialogActions>
    </Dialog>
  );
}
