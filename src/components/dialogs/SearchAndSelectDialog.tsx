// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import {
  Box,
  Button,
  ButtonGroup,
  Card,
  CardActionArea,
  CardContent,
  CardHeader,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  InputAdornment,
  List,
  ListItem,
  MenuItem,
  Pagination,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import RadioButtonUncheckedIcon from "@mui/icons-material/RadioButtonUnchecked";
import React, { useEffect } from "react";
import { SparqlEndpointFetcher } from "fetch-sparql-endpoint";
import * as N3 from "n3";
import type { Quad, Term } from "@rdfjs/types";

import { AR, O, P, R, RESULTS_PER_PAGE, S, T } from "../../utils/constants";

interface SparqlResult {
  [S]: Term;
  [AR]: Term;
}

interface SparqlBinding {
  [S]: Term;
  [P]: Term;
  [O]: Term;
}

/**
 * Computes the number of pages from the number of results and the results per page.
 * @param {number} numberOfResults
 * @param {number} resultsPerPage
 * @returns
 */
function computeNumberOfPages(numberOfResults: number, resultsPerPage: number) {
  //this emulates integer division and plus 1 for the rest, if any
  return (
    (numberOfResults - (numberOfResults % resultsPerPage)) / resultsPerPage +
    (numberOfResults % resultsPerPage ? 1 : 0)
  );
}

function sliceResults(
  searchResults: SparqlResult[],
  resultsPerPage: number,
  currentPage: number
) {
  return searchResults.slice(
    resultsPerPage * (currentPage - 1),
    resultsPerPage * currentPage
  );
}

interface SearchAndSelectDialogProps {
  /** Get and set the "open" state. */
  isOpen: [boolean, React.Dispatch<React.SetStateAction<boolean>>];
  /** The function to set the opened data with. */
  setData: React.Dispatch<React.SetStateAction<Quad[] | N3.Store>>;
  /** If true, allow a single selection only, else multiple. */
  single: boolean;
}

// TODO: implement search filter for NodeShapes in schema
/** Search a SPARQL store and open data from it. */
export default function SearchAndSelectDialog({
  isOpen: [open, setOpen],
  // dataGraph is never used and needless; reduce to a setData function that either creates a DataSetCore/Store or a Quad[]
  // useDataGraph: [dataGraph, setDataGraph],
  setData,
  single = false,
}: SearchAndSelectDialogProps) {
  /** The search term as typed in by the user. */
  const [userInput, setUserInput] = React.useState("");

  const [submittedUserInput, setSubmittedUserInput] = React.useState("");
  /** Indicates readiness of component during async loading of results. */
  const [loadingResults, setLoadingResults] = React.useState(false);
  /** Number of results to be displayed per page. */
  const [resultsPerPage, setResultsPerPage] = React.useState(
    RESULTS_PER_PAGE[1]
  );
  /** The currently selected page of the search result. */
  const [currentPage, setCurrentPage] = React.useState(1);
  /** The SPARQL result array */
  const [searchResults, setSearchResults] = React.useState<SparqlResult[]>([]);

  const [currentPagesTriples, setCurrentPagesTriples] = React.useState(
    new N3.Store()
  );

  // think about initializing with opened Ids, that are taken from dataGraph
  const [selectedIds, setSelectedIds] = React.useState<Set<string>>(new Set());
  const [selectedTriples, setSelectedTriples] = React.useState(new N3.Store());

  useEffect(() => {
    if (submittedUserInput)
      (async (): void => {
        const fetcher = new SparqlEndpointFetcher();
        // the variables in the select are bound to the results => field names
        const bindingsStream = await fetcher.fetchBindings(
          "/store/repositories/bdn",
          /* `PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
            SELECT ?${S} ?${P} ?${T}
            WHERE { ?${S} search:matches [
            search:property ?${P};
            search:query "${submittedUserInput}~1" ;
            search:snippet ?${T} ] }` */
          `
          PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
          SELECT ?${S} (CONCAT("[ ", GROUP_CONCAT(DISTINCT ?${R};   SEPARATOR=", "), " ]") AS ?${AR} )
          WHERE {
            SELECT ?${S} ?${R}
            WHERE { ?${S}
                search:matches [
                search:property ?${P};
                search:query "${submittedUserInput}~1" ;
                search:snippet ?${T} ] .
                BIND(CONCAT("{ \\"", STR(?${P}), "\\": \\"", STR(?${T}), "\\" }") AS ?${R}).
            }
            GROUP BY ?${S} ?${R}
          }
          GROUP BY ?${S}
          `
        );
        const store = [];
        bindingsStream
          .on("data", (binding) => {
            store.push(binding);
          })
          .on("finish", () => {
            console.log(store);
            setSearchResults(store);
          });
      })();
  }, [submittedUserInput]);

  useEffect(() => {
    if (searchResults.length)
      (async (): void => {
        setLoadingResults(true);
        // no need for removing duplicate ids, irrelevant for query (performance?)
        const valuesString = sliceResults(
          searchResults,
          resultsPerPage,
          currentPage
        ) //slicedResults
          .map((result) => `<${result[S].value}>`)
          .join(" ");
        const fetcher = new SparqlEndpointFetcher();
        // the variables in the select are bound to the results => field names
        const bindingsStream = await fetcher.fetchBindings(
          "/store/repositories/bdn",
          // no need to limit results because results already sliced
          `SELECT ?${S} ?${P} ?${O}
            WHERE {
              VALUES ?${S} { ${valuesString} }
            ?${S} ?${P} ?${O} }
          `
        );
        const n3Store = new N3.Store();
        bindingsStream
          .on("data", (binding: SparqlBinding) => {
            // => variable names are the ones from the query!!
            n3Store.addQuad(binding[S], binding[P], binding[O]);
          })
          .on("finish", () => {
            setCurrentPagesTriples(n3Store);
          });
        setLoadingResults(false);
      })();
  }, [currentPage, resultsPerPage, searchResults]);

  // **************
  // event handlers
  // **************

  /** Set search term according to user input. */
  function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    setUserInput(event.target.value);
  }

  /** Search on user pressing the enter key. */
  function handleKeyPress(event: React.ChangeEvent<HTMLInputElement>) {
    if (event.key === "Enter" && userInput) setSubmittedUserInput(userInput);
  }

  function handleClickSearchButton() {
    if (userInput) setSubmittedUserInput(userInput);
  }

  function handleClickCancelButton() {
    setOpen(false);
  }

  /** Load selected triples into dataGraph */
  function handleClickOpenButton() {
    if (single) {
      // return an array of quads of a single resource
      setData(selectedTriples.getQuads());
    } else {
      // return resource(s) as a Store
      setData(new N3.Store(selectedTriples.getQuads()));
    }
    setOpen(false);
  }

  /** Set the number of results per page on user selection. */
  function handleSelectResultsPerPage(
    event: React.ChangeEvent<HTMLInputElement>
  ) {
    setResultsPerPage(event.target.value);
    setCurrentPage(1);
  }

  /** Set currentPage to the new value after a page turn. */
  function onTurnPage(_event, value: number) {
    setCurrentPage(value);
  }

  function handleClickCard(id: string) {
    if (single) {
      console.log(id);
      setSelectedIds(new Set([id]));
      const newStore = new N3.Store();
      newStore.addQuads(currentPagesTriples.getQuads(id));
      setSelectedTriples(newStore);
    } else {
      const ids = new Set(selectedIds);
      if (ids.has(id)) ids.delete(id);
      else ids.add(id);
      setSelectedIds(ids);
      // solution with triples
      console.log(selectedTriples.size);
      const newStore = new N3.Store();
      newStore.addQuads([
        ...selectedTriples.getQuads(),
        ...currentPagesTriples.getQuads(id),
      ]);
      setSelectedTriples(newStore);
    }
  }

  return (
    <Dialog open={open} fullScreen>
      <Container>
        <DialogTitle>Search and select</DialogTitle>
        <TextField
          fullWidth
          label={"Search Graph Database Object Values"}
          helperText={undefined}
          type="search"
          value={userInput}
          onChange={handleChange}
          onKeyPress={handleKeyPress}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  aria-label="Search"
                  edge="end"
                  onClick={handleClickSearchButton}
                >
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Container>
      <DialogContent>
        {searchResults ? (
          <Container>
            <Stack direction="row" spacing={2} alignItems="center">
              <Box>Number of results: {searchResults.length}</Box>
              <TextField
                select
                value={resultsPerPage}
                onChange={handleSelectResultsPerPage}
              >
                {RESULTS_PER_PAGE.map((item) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </TextField>
            </Stack>
            <List>
              {sliceResults(searchResults, resultsPerPage, currentPage).map(
                (result) => (
                  <ListItem key={JSON.stringify(result)}>
                    <Card>
                      <CardActionArea
                        onClick={() => handleClickCard(result[S].value)}
                      >
                        <CardHeader
                          title={
                            <Stack direction="row" spacing={2}>
                              {/* Refactor to use selectedTriples */}
                              {selectedIds.has(result[S].value) ? (
                                <CheckCircleIcon />
                              ) : (
                                <RadioButtonUncheckedIcon />
                              )}
                              {JSON.parse(result[AR].value).map((item) => (
                                <Typography
                                  paragraph
                                  key={JSON.stringify(item)}
                                >
                                  <Box
                                    component="span"
                                    sx={{ typography: "body1" }}
                                  >
                                    {
                                      //TODO: lookup label
                                      Object.keys(item).pop()
                                    }
                                  </Box>
                                  <Typography
                                    variant="body1"
                                    component="span"
                                    /* This is safe because snippet can only contain <b>. */
                                    dangerouslySetInnerHTML={{
                                      __html: Object.values(item).pop(),
                                    }}
                                  />
                                </Typography>
                              ))}
                            </Stack>
                          }
                        />
                        <CardContent>
                          <Typography variant="body2">
                            {currentPagesTriples
                              .getObjects(result[S].value)
                              .filter((object) => object.value)
                              .map((object) => object.value)
                              .join(", ")}
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </ListItem>
                )
              )}
            </List>
          </Container>
        ) : (
          "No results."
        )}
      </DialogContent>
      <DialogActions>
        {searchResults.length ? (
          <Pagination
            count={computeNumberOfPages(searchResults.length, resultsPerPage)}
            page={currentPage}
            onChange={onTurnPage}
          />
        ) : (
          ""
        )}
        <ButtonGroup>
          <Button onClick={handleClickCancelButton}>Cancel</Button>
          <Button onClick={handleClickOpenButton} variant="contained">
            Open
          </Button>
        </ButtonGroup>
      </DialogActions>
    </Dialog>
  );
}
