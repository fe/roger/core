// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import NiceModal, { useModal } from "@ebay/nice-modal-react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";
import * as N3 from "n3";
import React, { useEffect, useState } from "react";

import { useRoger } from "../hooks";
import { PropertiesForm } from "./PropertiesForm";
import { SparqlConfig } from "../types/endpoints";

interface CreateNewFormModalProps {
  quads: N3.Quad[];
  store: N3.Store;
  onSuccess: () => Promise<void>;
  endpoints?: SparqlConfig;
  rootQuads: N3.Quad[];
  label: string;
}

export const CreateNewFormModal = NiceModal.create<
  Partial<CreateNewFormModalProps>
>(({ quads: defaultQuads, store, onSuccess, endpoints, rootQuads, label }) => {
  const [quads, setQuads] = useState(defaultQuads ?? []);
  const { visible, hide } = useModal();

  const { onSave } = useRoger();

  useEffect(() => {
    setQuads(defaultQuads ?? []);
  }, [defaultQuads]);

  const handleSave = async () => {
    if (endpoints) {
      await onSave?.(quads);
      await onSuccess?.();
      await hide();
    }
  };

  return (
    <Dialog open={visible} onClose={hide} fullWidth maxWidth="md">
      <DialogTitle>{label}</DialogTitle>
      <DialogContent>
        {!!store && !!endpoints && !!rootQuads && (
          <PropertiesForm
            quads={quads}
            onQuadsUpdate={setQuads}
            store={store}
            endpoints={endpoints}
            rootQuads={rootQuads}
          />
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={hide} color="primary">
          Cancel
        </Button>
        <Button onClick={handleSave} color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
});
