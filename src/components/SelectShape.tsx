// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import React from "react";
import dataFactory from "@rdfjs/data-model";
import type { Quad } from "@rdfjs/types";
import IRIs from "../utils/IRIs";

interface SelectShapeProps {
  onSelectShape;
  shapes: Quad[][];
  selectedShape?: Quad[];
}

export default function SelectShape({
  onSelectShape: handleSelectShape,
  shapes,
  selectedShape,
}: SelectShapeProps) {
  const SELECT_LABEL = "Shape";
  const SELECT_LABEL_ID = "select-shape-label";

  // Filter the shapes to include only those that do not have formNode == "false"
  const filteredShapes = shapes.filter(
    (shape) =>
      !shape.some(
        (q) =>
          q.predicate.equals(dataFactory.namedNode(IRIs.roger.formNode)) &&
          q.object.equals(
            dataFactory.literal(
              "false",
              dataFactory.namedNode(IRIs.xsd.boolean)
            )
          )
      )
  );

  return (
    <FormControl fullWidth>
      {/* MUI-specific: To properly label your Select input you need an extra element with an id that contains a label. That id needs to match the labelId of the Select e.g. */}
      <InputLabel id={SELECT_LABEL_ID}>{SELECT_LABEL}</InputLabel>
      <Select
        label={SELECT_LABEL}
        labelId={SELECT_LABEL_ID}
        value={selectedShape?.[0]?.subject.value || ""}
        onChange={(event) => handleSelectShape(event.target.value)}
      >
        {filteredShapes.map((shape) => (
          <MenuItem
            key={JSON.stringify(shape)}
            value={shape?.[0]?.subject.value}
          >
            {
              // look for shacl name; fall back to rdfs label if not found
              (
                shape.find((q) => q.predicate.value === IRIs.sh.name) ||
                shape.find((q) => q.predicate.value === IRIs.rdfs.label)
              )?.object.value ||
                // fall back so subject as string if neither is found
                shape?.[0]?.subject.value
            }
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
