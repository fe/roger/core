// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

import AutoCompleteEditor from "./editors/AutoCompleteEditor";
import BooleanSelectorEditor from "./editors/BooleanSelectEditor";
import DetailsEditor from "./editors/DetailsEditor";
import HannahArendtRichTextEditor from "./editors/HannahArendtRichTextEditor";
import RemoteInstancesSelectEditor from "./editors/RemoteInstancesSelectEditor";
import TextAreaEditor from "./editors/TextAreaEditor";
import TextFieldEditor from "./editors/TextFieldEditor";
import URIEditor from "./editors/URIEditor";

// TextFieldEditor is used as a fallback for components not implemented yet
const DEFAULT_EDITOR = TextFieldEditor;

export const componentMapping = {
  "http://datashapes.org/dash#AutoCompleteEditor": AutoCompleteEditor,
  "http://datashapes.org/dash#BlankNodeEditor": DEFAULT_EDITOR,
  "http://datashapes.org/dash#BooleanSelectEditor": BooleanSelectorEditor,
  "http://datashapes.org/dash#DatePickerEditor": DEFAULT_EDITOR,
  "http://datashapes.org/dash#DateTimePickerEditor": DEFAULT_EDITOR,
  "http://datashapes.org/dash#DetailsEditor": DetailsEditor,
  "http://datashapes.org/dash#EnumSelectEditor": DEFAULT_EDITOR,
  "http://datashapes.org/dash#InstancesSelectEditor": DEFAULT_EDITOR,
  "http://datashapes.org/dash#RichTextEditor": DEFAULT_EDITOR,
  "http://datashapes.org/dash#SubClassEditor": DEFAULT_EDITOR,
  "http://datashapes.org/dash#TextAreaEditor": TextAreaEditor,
  "http://datashapes.org/dash#TextAreaWithLangEditor": DEFAULT_EDITOR,
  "http://datashapes.org/dash#TextFieldEditor": TextFieldEditor,
  "http://datashapes.org/dash#TextFieldWithLangEditor": DEFAULT_EDITOR,
  "http://datashapes.org/dash#URIEditor": URIEditor,
  "https://www.sub.uni-goettingen.de/roger/schema#SelectRemoteInstanceEditor":
    RemoteInstancesSelectEditor,
  "https://www.sub.uni-goettingen.de/roger/schema#HannahArendtRichTextEditor":
    HannahArendtRichTextEditor,
};
