// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { Stack } from "@mui/material";
import React from "react";

import {
  getPropertyValue,
  getSelectedShape,
  getShape,
  getShapeProperties,
} from "../utils";
import IRIs from "../utils/IRIs";
import EditorFrame from "./editors/EditorFrame";
import FieldFrame from "./editors/FieldFrame";
import { componentMapping } from "./mapping";

export default function ShapeForm({
  data: dataQuads,
  onChange,
  schema: schemaStore,
}) {
  const nodeShape = getSelectedShape(schemaStore, dataQuads);
  /**
   * An Array of the shape objects for each PropertyShape defined as a property of the
   * selected NodeShape.
   * ATTENTION: This is not a duplicate implementation of utils/getters/getPropertyShapes.
   */
  const propertyShapes = getShapeProperties(schemaStore, nodeShape)?.map((q) =>
    getShape(schemaStore, q.object)
  );

  return (
    <Stack spacing={3} sx={{ width: "100%" }}>
      {propertyShapes?.map((propertyShape) => (
        <EditorFrame
          data={dataQuads}
          key={JSON.stringify(propertyShape)}
          onChange={onChange}
          path={getPropertyValue(propertyShape, IRIs.sh.path)}
          label={getPropertyValue(propertyShape, IRIs.sh.name)}
          helperText={getPropertyValue(propertyShape, IRIs.sh.description)}
          shape={propertyShape}
        >
          {dataQuads
            .filter(
              (q) =>
                q.predicate.value ===
                getPropertyValue(propertyShape, IRIs.sh.path)
            )
            .map((q, index) => (
              <FieldFrame
                index={index}
                key={JSON.stringify(propertyShape) + index}
                path={getPropertyValue(propertyShape, IRIs.sh.path)}
                onChange={onChange}
                quad={q}
              >
                {React.createElement(
                  componentMapping[
                    getPropertyValue(
                      propertyShape,
                      "http://datashapes.org/dash#editor"
                    )
                  ],
                  {
                    // hand down an array of quads with the BlankNode's identifier as subject, value otherwise
                    data:
                      q.object.termType === "BlankNode"
                        ? dataQuads.filter((quad) =>
                            quad.subject.equals(q.object)
                          )
                        : q.object.value,
                    onChange: onChange,
                    path: getPropertyValue(propertyShape, IRIs.sh.path),
                    quad: q,
                    schema: schemaStore,
                  }
                )}
              </FieldFrame>
            ))}
        </EditorFrame>
      ))}
    </Stack>
  );
}
