// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import * as N3 from "n3";
import React from "react";

import { SparqlConfig } from "../../types/endpoints";
import { PropertiesForm } from "../PropertiesForm";

interface DetailsEditorProps {
  data: N3.Quad[];
  onChange: {
    handleQuadsUpdate: (data: N3.Quad[]) => void;
  };
  schema: N3.Store;
  endpoints: SparqlConfig;
  rootQuads: N3.Quad[];
}

const DetailsEditor: React.FC<DetailsEditorProps> = ({
  data,
  onChange,
  schema,
  endpoints,
  rootQuads,
}) => {
  return (
    <PropertiesForm
      quads={data}
      onQuadsUpdate={onChange.handleQuadsUpdate}
      store={schema}
      endpoints={endpoints}
      rootQuads={rootQuads}
    />
  );
};

export default DetailsEditor;
