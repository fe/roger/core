// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React, { FC } from "react";
import { CharacterMetadata, ContentBlock, ContentState } from "draft-js";
import { additionalDraftStyles } from "./styles";

export const findLanguageEntities = (
  contentBlock: ContentBlock,
  callback: (start: number, end: number) => void,
  contentState: ContentState
) => {
  contentBlock.findEntityRanges(
    (character: CharacterMetadata) => {
      const entityKey = character.getEntity();
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === "LANGUAGE"
      );
    },
    (start: number, end: number) => {
      callback(start, end);
    }
  );
};

interface LanguageProps {
  contentState: ContentState;
  entityKey: string;
  children: React.ReactNode;
}

const styles: Record<
  string,
  { draftName: keyof typeof additionalDraftStyles }
> = {
  en: { draftName: "RED" },
  de: { draftName: "PINK" },
  "grc-la": { draftName: "SEPIA" },
  grc: { draftName: "BLUE" },
  fr: { draftName: "GREEN" },
  la: { draftName: "ORANGE" },
};

export const Language: FC<LanguageProps> = ({
  contentState,
  entityKey,
  children,
}) => {
  const { lang } = contentState.getEntity(entityKey).getData();
  //const lang ="de"
  const draftName = styles[lang]?.draftName;
  const style = draftName ? additionalDraftStyles[draftName] : {};

  return (
    <span
      // props contains a contentState object, which is not suitable as an attribute for a span...
      //{...props}
      data-lang={lang}
      style={{
        ...style,
        fontStyle: "italic",
        borderBottom: "1px dotted #000",
      }}
    >
      {children}
    </span>
  );
};
