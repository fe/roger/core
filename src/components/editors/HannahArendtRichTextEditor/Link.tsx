// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React, { FC } from "react";

import { ContentBlock, ContentState } from "draft-js";
import { additionalDraftStyles } from "./styles";

export const findLinkEntities = (
  contentBlock: ContentBlock,
  callback: (start: number, end: number) => void,
  contentState: ContentState
) => {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity();
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === "LINK"
      );
    },
    (start: number, end: number) => {
      callback(start, end);
    }
  );
};

interface LinkProps {
  contentState: ContentState;
  entityKey: string;
  children: React.ReactNode;
}

export const Link: FC<LinkProps> = ({ contentState, entityKey, children }) => {
  const { url } = contentState.getEntity(entityKey).getData();
  return (
    <a href={url} style={additionalDraftStyles.LINK}>
      {children}
    </a>
  );
};
