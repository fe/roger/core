// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React, { useCallback, useEffect, useState } from "react";

import {
  CompositeDecorator,
  DraftHandleValue,
  Editor,
  EditorState,
  RichUtils,
} from "draft-js";
import "draft-js/dist/Draft.css";
import { convertFromHTML, convertToHTML } from "draft-convert";

import {
  Box,
  Button,
  debounce,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  FormControl,
  MenuItem,
  Select,
  Stack,
  TextField,
  ToggleButton,
  ToggleButtonGroup,
  Tooltip,
} from "@mui/material";

import {
  AddLink as AddLinkIcon,
  FormatBold as FormatBoldIcon,
  FormatItalic as FormatItalicIcon,
  FormatStrikethrough as FormatStrikethroughIcon,
  FormatUnderlined as FormatUnderlinedIcon,
  LinkOff as LinkOffIcon,
} from "@mui/icons-material";

import { Quad } from "@rdfjs/types";
import { findLinkEntities, Link } from "./Link";
import { findLanguageEntities, Language } from "./Language";
import { additionalDraftStyles } from "./styles";

interface RichTextEditorProps {
  data: string;
  onChange: {
    handleChange: (html: string, quad: Quad) => void;
  };
  quad: Quad;
}

interface LinkState {
  showURLInput: boolean;
  urlValue: string;
}

// a decorator for draft entities
const compositeDecorator = new CompositeDecorator([
  { strategy: findLinkEntities, component: Link },
  { strategy: findLanguageEntities, component: Language },
]);

// map inline styles with their draftjs names
const inlineStyles: Record<string, { draftName: string }> = {
  bold: { draftName: "BOLD" },
  italic: { draftName: "ITALIC" },
  strikethrough: { draftName: "STRIKETHROUGH" },
  underline: { draftName: "UNDERLINE" },
};

// hard coded language map
const languages: Record<string, string> = {
  en: "english",
  de: "german",
  grc: "greek",
  fr: "french",
  la: "latin",
};

// map for conversion from ContentState to "HTML", i.e. what you see rendered in the text field
const conversionMap = {
  styleToHTML: (style: string) => {
    const tagMap: Record<string, string> = {
      BOLD: "bold",
      ITALIC: "italic",
      STRIKETHROUGH: "strikethrough",
      UNDERLINE: "underline",
    };

    return React.createElement("hi", { rend: tagMap[style] });
  },
  blockToHTML: (block: { type: string }) => {
    if (block.type === "unstyled") {
      return React.createElement("p", { xmlns: "http://www.tei-c.org/ns/1.0" });
    }
  },
  entityToHTML: (entity: any, originalText: string) => {
    if (entity.type === "LINK") {
      return React.createElement("a", { href: entity.data.url }, originalText);
    }
    if (entity.type === "LANGUAGE") {
      const xmlLangAttr = document.createAttributeNS(
        "http://www.w3.org/XML/1998/namespace",
        "lang"
      );
      xmlLangAttr.value = entity.data.lang;

      const foreignElement = document.createElementNS(
        "http://www.tei-c.org/ns/1.0",
        "foreign"
      );
      foreignElement.setAttributeNode(xmlLangAttr);
      foreignElement.textContent = originalText;

      const serializer = new XMLSerializer();
      return serializer.serializeToString(foreignElement);
    }
    return originalText;
  },
};

// map for conversion from "HTML" to ContentState, i.e. what will be converted from the input data
const conversionMapReverse = {
  htmlToStyle: (
    nodeName: string,
    node: HTMLElement,
    currentStyle: Set<string>
  ) => {
    const styleMap: Record<string, string> = {
      bold: "BOLD",
      italic: "ITALIC",
      strikethrough: "STRIKETHROUGH",
      underline: "UNDERLINE",
    };
    if (nodeName === "hi") {
      const style = styleMap[node.getAttribute("rend")!];

      if (style) {
        return currentStyle.add(style);
      }
    }
    return currentStyle;
  },
  htmlToEntity: (
    nodeName: string,
    node: HTMLElement,
    createEntity: (type: string, mutability: string, data: any) => string
  ) => {
    if (nodeName === "a") {
      return createEntity("LINK", "MUTABLE", {
        url: node.getAttribute("href"),
      });
    }
    if (nodeName === "foreign") {
      return createEntity("LANGUAGE", "MUTABLE", {
        lang: node.getAttribute("xml:lang")!,
      });
    }
    return "";
  },
};

const HannahArendtRichTextEditor: React.FC<RichTextEditorProps> = ({
  data,
  onChange: { handleChange: onChange },
  quad,
}) => {
  // states
  const [editorState, setEditorState] = useState(
    EditorState.createEmpty(compositeDecorator)
  );
  const [languageState, setLanguageState] = useState<string>("");
  const [linkState, setLinkState] = useState<LinkState>({
    showURLInput: false,
    urlValue: "",
  });

  useEffect(() => {
    const contentState = convertFromHTML(conversionMapReverse)(data);
    setEditorState(
      contentState
        ? EditorState.createWithContent(contentState, compositeDecorator)
        : EditorState.createEmpty(compositeDecorator)
    );
  }, [data]);

  // handlers
  const handleKeyCommand = (
    command: string,
    editorState: EditorState
  ): DraftHandleValue => {
    const newState = RichUtils.handleKeyCommand(editorState, command);

    if (newState) {
      setEditorState(newState);
      return "handled";
    }

    return "not-handled";
  };

  const handleInlineStyleChange = (style: string) => {
    setEditorState(
      RichUtils.toggleInlineStyle(editorState, inlineStyles[style].draftName)
    );
  };

  // LINK event handlers
  const promptForLink = (event: React.MouseEvent) => {
    event.preventDefault();
    const selection = editorState.getSelection();
    if (!selection.isCollapsed()) {
      const contentState = editorState.getCurrentContent();
      const startKey = editorState.getSelection().getStartKey();
      const startOffset = editorState.getSelection().getStartOffset();
      const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
      const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);

      let url = "";
      if (linkKey) {
        const linkInstance = contentState.getEntity(linkKey);
        url = linkInstance.getData().url;
      }

      setLinkState({
        showURLInput: true,
        urlValue: url,
      });
    }
  };

  const handleCloseLink = () =>
    setLinkState((prevState) => ({
      ...prevState,
      showURLInput: false,
    }));

  const confirmLink = (event: React.MouseEvent) => {
    event.preventDefault();
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      "LINK",
      "MUTABLE",
      { url: linkState.urlValue }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, {
      currentContent: contentStateWithEntity,
    });

    setEditorState(
      RichUtils.toggleLink(
        newEditorState,
        newEditorState.getSelection(),
        entityKey
      )
    );
    setLinkState({
      showURLInput: false,
      urlValue: "",
    });
  };

  const onLinkInputKeyDown = (event: React.KeyboardEvent) => {
    if (event.key === "Enter") {
      confirmLink(event as unknown as React.MouseEvent);
    }
  };

  const onURLChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLinkState((prevState) => ({
      ...prevState,
      urlValue: event.target.value,
    }));
  };

  const removeLink = (event: React.MouseEvent) => {
    event.preventDefault();
    const selection = editorState.getSelection();
    if (!selection.isCollapsed()) {
      setEditorState(RichUtils.toggleLink(editorState, selection, null));
    }
  };

  const handleEditorChange = useCallback(
    debounce((newEditorState: EditorState) => {
      setEditorState(newEditorState);
      const content = newEditorState.getCurrentContent();
      const html = convertToHTML(conversionMap)(content);
      onChange(html, quad);
    }, 300),
    [quad, onChange]
  );

  // change language event handlers
  const handleLanguageChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    event.preventDefault();

    const lang = event.target.value as string;
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      "LANGUAGE",
      "MUTABLE",
      { lang }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, {
      currentContent: contentStateWithEntity,
    });

    setEditorState(
      RichUtils.toggleLink(
        newEditorState,
        newEditorState.getSelection(),
        entityKey
      )
    );

    setLanguageState(lang);
  };

  // DEBUG
  /* const logState = () => {
    const content = editorState.getCurrentContent();
    const html = convertToHTML(conversionMap)(content);
    console.log(html);
    console.dir(convertToRaw(content));
  }; */

  return (
    <Stack spacing={2} flex={1}>
      <Stack direction="row" spacing={1}>
        <ToggleButtonGroup size="small">
          <Tooltip title={"editor.bold.tooltip"}>
            <ToggleButton
              value="bold"
              onClick={() => handleInlineStyleChange("bold")}
            >
              <FormatBoldIcon />
            </ToggleButton>
          </Tooltip>
          <Tooltip title={"editor.italic.tooltip"}>
            <ToggleButton
              value="italic"
              onClick={() => handleInlineStyleChange("italic")}
            >
              <FormatItalicIcon />
            </ToggleButton>
          </Tooltip>
          <Tooltip title={"editor.strikethrough.tooltip"}>
            <ToggleButton
              value="strikethrough"
              onClick={() => handleInlineStyleChange("strikethrough")}
            >
              <FormatStrikethroughIcon />
            </ToggleButton>
          </Tooltip>
          <Tooltip title={"editor.underline.tooltip"}>
            <ToggleButton
              value="underline"
              onClick={() => handleInlineStyleChange("underline")}
            >
              <FormatUnderlinedIcon />
            </ToggleButton>
          </Tooltip>
        </ToggleButtonGroup>
        <Divider orientation="vertical" flexItem />
        <ToggleButtonGroup size="small">
          {
            // TODO: switch between Add and Edit link
          }

          <Tooltip title={"editor.addLink.tooltip"}>
            <ToggleButton value="addlink" onClick={promptForLink}>
              <AddLinkIcon />
            </ToggleButton>
          </Tooltip>
          <Dialog
            open={linkState.showURLInput}
            onClose={handleCloseLink}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">
              {"editor.linkDialog.title"}
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                {"editor.linkDialog.description"}
              </DialogContentText>
              <TextField
                margin="dense"
                fullWidth
                name="id"
                label={"editor.linkDialog.inputLabel"}
                onChange={onURLChange}
                onKeyDown={onLinkInputKeyDown}
                value={linkState.urlValue}
                size="small"
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseLink} color="primary">
                {"button.cancel.title"}
              </Button>
              <Button onClick={confirmLink} color="primary" variant="contained">
                {"editor.linkDialog.title"}
              </Button>
            </DialogActions>
          </Dialog>
          <Tooltip title={"editor.removeLink.tooltip"}>
            <ToggleButton value="rmlink" onMouseDown={removeLink}>
              <LinkOffIcon />
            </ToggleButton>
          </Tooltip>
        </ToggleButtonGroup>
        <Divider orientation="vertical" flexItem />
        <FormControl size="small" sx={{ width: 120 }}>
          {/* <InputLabel id="demo-simple-select-label">{"note.language.title"}</InputLabel> */}
          <Select
            defaultValue=""
            id="demo-simple-select"
            value={languageState}
            label="Language"
            labelId="demo-simple-select-label"
            onChange={handleLanguageChange}
          >
            {Object.entries(languages).map(([code, name]) => (
              <MenuItem key={code} value={code}>
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        {/* <Button onMouseDown={logState}>LOG</Button> */}
      </Stack>

      {/* Matching MUI styles as closesly as possible for non-MUI editor component */}
      <Box
        sx={[
          (theme) => ({
            border: "1px solid rgba(0, 0, 0, 0.23)", // This is actually hardcoded in MUI, so no var available
            borderRadius: 1,
            "&:hover:not(:focus-within)": {
              borderColor: "inherit",
            },
            "&:focus-within": {
              borderColor: theme.palette.primary.main,
              boxShadow: `0 0 0 1px inset ${theme.palette.primary.main}`,
            },
            ".public-DraftEditor-content": {
              padding: "16.5px 14px",
            },
          }),
        ]}
      >
        <Editor
          editorState={editorState}
          handleKeyCommand={handleKeyCommand}
          onChange={handleEditorChange}
          customStyleMap={additionalDraftStyles}
        />
      </Box>
    </Stack>
  );
};

export default HannahArendtRichTextEditor;
