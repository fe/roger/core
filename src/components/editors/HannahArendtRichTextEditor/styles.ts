// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

import React from "react";

/**
 * Additional styles used for rendering in the `Editor` component.
 * Built-in are mappings for 'BOLD', 'ITALIC', 'UNDERLINE', and 'CODE',
 * see https://draftjs.org/docs/advanced-topics-inline-styles/#mapping-a-style-string-to-css.
 */
export const additionalDraftStyles: Record<string, React.CSSProperties> = {
  LINK: {
    color: "#3b5998",
    textDecoration: "underline",
  },
  STRIKETHROUGH: {
    textDecoration: "line-through",
  },
  BLUE: {
    color: "#0000ff",
  },
  RED: {
    color: "#ff0000",
  },
  PINK: {
    color: "#fc02ff",
  },
  ORANGE: {
    color: "#f79646",
  },
  GREEN: {
    color: "#00ff00",
  },
  SEPIA: {
    color: "#704214",
  },
};
