// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import CheckIcon from "@mui/icons-material/Check";
import ClearIcon from "@mui/icons-material/Clear";
import { ToggleButton, ToggleButtonGroup } from "@mui/material";
import * as N3 from "n3";
import React from "react";

interface BooleanSelectEditorProps {
  quad: N3.Quad;
  onChange: {
    handleChange: (value: string, quad: N3.Quad) => void;
  };
}

const BooleanSelectorEditor: React.FC<BooleanSelectEditorProps> = ({
  quad,
  onChange,
}) => {
  return (
    <ToggleButtonGroup
      value={quad.object.value}
      exclusive
      onChange={(_, value) => {
        onChange.handleChange(value, quad);
      }}
    >
      <ToggleButton value="false">
        <ClearIcon />
      </ToggleButton>

      <ToggleButton value="true">
        <CheckIcon />
      </ToggleButton>
    </ToggleButtonGroup>
  );
};

export default BooleanSelectorEditor;
