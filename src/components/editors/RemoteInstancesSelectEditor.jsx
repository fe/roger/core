// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React from "react";

import { IconButton, InputAdornment, TextField } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import RemoteInstancesSelectDialog from "./RemoteInstancesSelectDialog";

//TODO: auto-fill on "paste" of authority-uri
export default function RemoteInstancesSelectEditor({
  disabled,
  shape,
  onChange: { handleChange },
  data,
  dataType,
  helperText,
  index,
  label,
  path,
}) {
  // ******
  // states
  // ******

  /** Visibility of the search result dialog. */
  const [open, setOpen] = React.useState(false);

  // **************
  // event handlers
  // **************

  /** Open dialog. */
  const handleOpenDialog = () => {
    setOpen(true);
  };

  /** Set the item chosen by the user from the search results and enrich it with the index of the text field. */
  const handleSelect = (itemId) => {
    handleChange(path, itemId, dataType, index);
  };

  return (
    <>
      <TextField
        disabled={disabled}
        label={label}
        helperText={helperText}
        type="search"
        value={data}
        onChange={(event) =>
          handleChange(path, event.target.value, dataType, index)
        }
        InputProps={{
          endAdornment: (
            <InputAdornment position="end" disabled={disabled}>
              <IconButton
                disabled={disabled}
                aria-label="Search"
                onClick={handleOpenDialog}
                edge="end"
              >
                <SearchIcon />
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      <RemoteInstancesSelectDialog
        handleSelect={handleSelect}
        isOpen={[open, setOpen]}
        shape={shape}
      />
    </>
  );
}
