// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import {
  Box,
  Button,
  CircularProgress,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  InputAdornment,
  List,
  ListItem,
  MenuItem,
  Pagination,
  Paper,
  Stack,
  TextField,
  Tooltip,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import DOMPurify from "dompurify";
import React, { useEffect } from "react";
import {
  KEY_CONTEXT,
  KEY_ID,
  KEY_TYPE,
  RESULTS_PER_PAGE,
} from "../../utils/constants";
import IRIs from "../../utils/IRIs";
import {
  getLobidGNDJsonldContext,
  getLobidPreview,
  getLobidResourcesJsonldContext,
  queryLobidGND,
} from "../../utils/queryAuthorities";
import useLocalStorage from "../../hooks/useLocalStorage";

// replace deprecated and removed import
const getKeyByValue = (obj, val) => {
  return Object.keys(obj).find((key) => obj[key] === val);
};

//TODO: implement auto-complete, filters and results ordering
export default function RemoteInstancesSelectDialog({
  handleSelect,
  isOpen,
  shape,
}) {
  /** The search term as typed in by the user. */
  const [userInput, setUserInput] = React.useState("");
  /** The search term as it is set on keypress or click. */
  const [searchTerm, setSearchTerm] = React.useState("");
  /** Array of number of results and the result objects. */
  const [searchResults, setSearchResults] = React.useState();
  /** Indicates readiness of component during async loading of results. */
  const [loadingResults, setLoadingResults] = React.useState(false);
  /** Indicates readiness of component during async loading of preview. */
  const [loadingPreview, setLoadingPreview] = React.useState(false);
  /** Visibility of the search result dialog. */
  const [open, setOpen] = isOpen;
  /** Number of results to be displayed per page. */
  const [resultsPerPage, setResultsPerPage] = React.useState(10);
  /** The currently selected page of the search result. */
  const [currentPage, setCurrentPage] = React.useState(1);

  const [currentlyHoveredItem, setCurrentlyHoveredItem] = React.useState();
  const [previewItem, setPreviewItem] = React.useState("");

  /** The JSON-LD context of the data fetched from the lobid API. */
  const [lobidGNDJsonldContext, setLobidGNDJsonldContext] = useLocalStorage(
    "lobidGNDJsonldContext",
    undefined
  );
  const [lobidResourcesJsonldContext, setLobidResourcesJsonldContext] =
    useLocalStorage("lobidResourcesJsonldContext", undefined);

  // *************************************
  // effects running on component mount only
  // *************************************
  // TODO: move this down to Dialog component
  useEffect(() => {
    /** Load and set the jsonldContext of the lobid GND API. */
    if (!lobidGNDJsonldContext)
      (async () => {
        const contextResponse = await getLobidGNDJsonldContext();
        setLobidGNDJsonldContext(contextResponse.data[KEY_CONTEXT]);
      })();
  }, [lobidGNDJsonldContext, setLobidGNDJsonldContext]);

  useEffect(() => {
    /** Load and set the jsonldContext of the lobid Resources API. */
    if (!lobidResourcesJsonldContext)
      (async () => {
        const contextResponse = await getLobidResourcesJsonldContext();
        setLobidResourcesJsonldContext(contextResponse.data[KEY_CONTEXT]);
      })();
  }, [lobidResourcesJsonldContext, setLobidResourcesJsonldContext]);

  // *************************************
  // effects depending on states's changes
  // *************************************

  /** Reset currrent page when search term has changed. */
  useEffect(() => {
    setCurrentPage(1);
  }, [searchTerm]);

  /** Load search results when the dialog is open or the current page changes. */
  useEffect(() => {
    if (open && searchTerm) {
      const filterName = getKeyByValue(lobidGNDJsonldContext, KEY_TYPE);
      // the filterTerm must be chosen depending on the NodeShape
      // thus the gndo corresponding to the NodeShape must be a property of it
      // so that ROGER can be agnostic of this mapping
      // temporal solution: see https://gitlab.gwdg.de/fe/roger-frontend/-/issues/9
      // with respect to propertyShapes also using this dialog with the corresponding editor, filterTerm should be handed down from the parent component
      const filterTerm = getKeyByValue(
        lobidGNDJsonldContext,
        shape?.[IRIs.roger.filterTerm]?.[KEY_ID]
      );
      const fromResultNr = (currentPage - 1) * resultsPerPage + 1;
      (async () => {
        setLoadingResults(true);
        const response = await queryLobidGND({
          searchTerm: searchTerm,
          filterName: filterName,
          filterTerm: filterTerm,
          from: fromResultNr,
          size: resultsPerPage,
        });
        if (response.status === 200) setSearchResults(response.data);
        setLoadingResults(false);
      })();
    }
  }, [
    currentPage,
    lobidGNDJsonldContext,
    open,
    resultsPerPage,
    searchTerm,
    shape,
  ]);

  /** Load and show a preview item on the result list. */
  useEffect(() => {
    if (currentlyHoveredItem)
      (async () => {
        setLoadingPreview(true);
        const response = await getLobidPreview(currentlyHoveredItem);
        const pure = DOMPurify.sanitize(response?.data, {
          USE_PROFILES: { html: true },
        });
        setPreviewItem(pure);
        setLoadingPreview(false);
      })();
  }, [currentlyHoveredItem]);

  // **************
  // event handlers
  // **************

  /** Set search term according to user input. */
  const handleChange = (event) => {
    setUserInput(event.target.value);
  };

  /** Close dialog. */
  const handleClose = () => {
    setOpen(false);
  };

  /** Set the number of results per page on user selection. */
  const handleSelectResultsPerPage = (event) => {
    setResultsPerPage(event.target.value);
  };

  /** Set currentPage to the new value after a page turn. */
  const onTurnPage = (_event, value) => {
    setCurrentPage(value);
  };

  /** Search on user pressing the enter key. */
  const handleKeyPress = (event) => {
    if (event.key === "Enter") setSearchTerm(event.target.value);
  };

  const handleListItemClick = (itemId) => {
    handleSelect(itemId);
    setOpen(false);
  };

  const handleListItemMouseEnter = (id) => {
    setCurrentlyHoveredItem(id.split("/").pop());
  };

  return (
    <Dialog onClose={handleClose} open={open} fullScreen>
      <DialogTitle>Search and select</DialogTitle>
      <Container>
        <TextField
          label={"Search"}
          helperText={undefined}
          type="search"
          value={userInput}
          onChange={handleChange}
          onKeyPress={handleKeyPress}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton aria-label="Search" edge="end">
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Container>
      {loadingResults ? (
        <Container>
          <CircularProgress />
        </Container>
      ) : (
        <DialogContent>
          {searchResults?.totalItems ? (
            <Container>
              <Stack direction="row" spacing={2} alignItems="center">
                <Box>Number of results: {searchResults.totalItems}</Box>
                <TextField
                  select
                  value={resultsPerPage}
                  onChange={handleSelectResultsPerPage}
                >
                  {RESULTS_PER_PAGE.map((item) => (
                    <MenuItem key={item} value={item}>
                      {item}
                    </MenuItem>
                  ))}
                </TextField>
              </Stack>
              <List>
                {searchResults.member.map((item) => (
                  <Tooltip
                    arrow
                    key={JSON.stringify(item)}
                    title={
                      <Paper>
                        {loadingPreview ? (
                          <Container>
                            <CircularProgress />
                          </Container>
                        ) : (
                          <div
                            // this is safe because previewItem has been sanitized
                            dangerouslySetInnerHTML={{ __html: previewItem }}
                          />
                        )}
                      </Paper>
                    }
                  >
                    <ListItem
                      button
                      onClick={() => handleListItemClick(item.id)} // the field id is specific to lobid => get it from jsonld context ("@id")
                      onMouseEnter={() => handleListItemMouseEnter(item.id)} // see above
                    >
                      {item.preferredName}
                    </ListItem>
                  </Tooltip>
                ))}
              </List>
            </Container>
          ) : (
            "No results."
          )}
        </DialogContent>
      )}
      <DialogActions>
        {searchResults ? (
          <Pagination
            // how many pages? this emulates integer division and plus 1 for the rest
            count={
              (searchResults.totalItems -
                (searchResults.totalItems % resultsPerPage)) /
                resultsPerPage +
              1
            }
            page={currentPage}
            onChange={onTurnPage}
          />
        ) : (
          ""
        )}
        <Button onClick={handleClose}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
}
