// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { IconButton, Stack } from "@mui/material";
import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";

/**
 * A component framing one input field of different kinds and the delete button.
 */
export default function FieldFrame({
  children,
  onChange: { handleRemove },
  quad,
}) {
  return (
    <Stack direction="row" spacing={2}>
      {children}
      <Stack paddingY={1}>
        <IconButton onClick={() => handleRemove(quad)}>
          <DeleteIcon />
        </IconButton>
      </Stack>
    </Stack>
  );
}
