// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import NiceModal from "@ebay/nice-modal-react";
import { Autocomplete, Box, Button, Stack, TextField } from "@mui/material";
import dataFactory from "@rdfjs/data-model";
import axios from "axios";
import * as N3 from "n3";
import React, { useEffect, useMemo, useState } from "react";
import { v4 as uuid } from "uuid";

import { SparqlConfig } from "../../types/endpoints";
import IRIs from "../../utils/IRIs";
import { CreateNewFormModal } from "../CreateNewFormModal";

type BindingValue = {
  type: string;
  value: string;
};

type Binding = {
  [key: string]: BindingValue;
};

type SparqlResponse = {
  results: {
    bindings: Binding[];
  };
};

type MappedResponseItem = {
  value: string;
  label: string;
};

interface AutoCompleteEditorProps {
  data: N3.Quad[];
  quad: N3.Quad;
  onChange: {
    handleChange: (value: string, quad: N3.Quad) => void;
  };
  schema: N3.Store;
  endpoints?: SparqlConfig;
  path: string;
  property: N3.Quad;
  rootQuads: N3.Quad[];
  label: string;
}

const AutoCompleteEditor: React.FC<AutoCompleteEditorProps> = ({
  schema,
  property,
  onChange,
  endpoints,
  quad,
  rootQuads,
  label,
}) => {
  const [items, setItems] = useState<MappedResponseItem[]>([]);
  const [selectedValue, setSelectedValue] = useState<MappedResponseItem | null>(
    null
  );

  const objectQuads = useMemo(
    () => schema.getQuads(property.object, null, null, null),
    [property.object, schema]
  );

  const endpoint = useMemo(() => {
    const shapeEndpoint = objectQuads.find(
      (el) => el.predicate.value === IRIs.roger.sparqlEndpoint
    )?.object.value;

    return shapeEndpoint ? shapeEndpoint : endpoints?.queryEndpoint;
  }, [objectQuads, endpoints]);

  const statement = useMemo(
    () =>
      objectQuads.find(
        (el) => el.predicate.value === IRIs.roger.sparqlStatement
      )?.object?.value,
    [objectQuads]
  );

  useEffect(() => {
    if (!endpoint || !statement) {
      return;
    }

    const loadData = async () => {
      const newItems = await fetchData(endpoint, statement);

      setItems(newItems);
    };

    loadData();
  }, [endpoint, statement]);

  const fetchData = async (
    endpoint: string,
    statement: string
  ): Promise<MappedResponseItem[]> => {
    if (!(endpoint && statement)) {
      return [];
    }

    try {
      const response = await axios.post<SparqlResponse>(endpoint, statement, {
        headers: {
          "Content-Type": "application/sparql-query",
          Accept: "application/sparql-results+json",
        },
      });

      const bindings = response.data.results?.bindings ?? [];

      const searchValueField =
        objectQuads.find((el) => el.predicate.value === IRIs.roger.searchValue)
          ?.object?.value ?? "";

      const searchLabelField =
        objectQuads.find((el) => el.predicate.value === IRIs.roger.searchLabel)
          ?.object?.value ?? "";

      const mapBindingToItem = (el: Binding): MappedResponseItem => {
        const value = el[searchValueField]?.value || "Unknown Value";
        const label = el[searchLabelField]?.value || "Unknown Label";

        return { value, label };
      };

      return bindings.map(mapBindingToItem);
    } catch (error) {
      console.error("Error fetching data:", error);
      return [];
    }
  };

  const modalId = useMemo(() => `${uuid()}`, []);

  const handleAddNewResource = async (
    modalId: string,
    endpoint: string | undefined,
    statement: string | undefined
  ) => {
    const propertyQuads = schema.getQuads(property.object, null, null, null);
    const referenceNode = propertyQuads.find(
      (el) => el.predicate.value === IRIs.sh.node
    );

    if (!referenceNode) {
      return null;
    }

    const referenceNodeQuads = schema.getQuads(
      referenceNode.object.value,
      null,
      null,
      null
    );

    const subject = referenceNodeQuads.find(
      (el) => el.predicate.value === IRIs.rdf.type
    );

    if (!subject) {
      return null;
    }

    const targetClass = schema
      .getObjects(subject.subject.value, IRIs.sh.targetClass, null)
      .pop();

    if (!targetClass) {
      return null;
    }

    const passageURI = `${targetClass.value}_${modalId}`;

    const containingNode = dataFactory.quad(
      dataFactory.namedNode(passageURI),
      dataFactory.namedNode(IRIs.rdf.type),
      dataFactory.namedNode(targetClass.value)
    );

    await NiceModal.show(modalId, {
      quads: [containingNode],
      store: schema,
      endpoints: endpoints,
      rootQuads: rootQuads,
      label: label,
      onSuccess: async () => {
        if (endpoint && statement) {
          const newItems = await fetchData(endpoint, statement);

          setItems(newItems);
        }
      },
    });
  };

  useEffect(() => {
    if (items.length && quad.object.value) {
      setSelectedValue(
        items.find((el) => el.value === quad.object.value) ?? null
      );
    }
  }, [items, quad.object.value]);

  return (
    <Box>
      <Stack direction="row" spacing={2}>
        <Autocomplete
          disablePortal
          id="combo-box-demo"
          options={items}
          getOptionLabel={(item) => item.label}
          value={selectedValue}
          onChange={(_, value) => {
            setSelectedValue(value);
            if (value) {
              onChange.handleChange(value.value, quad);
            }
          }}
          sx={{ width: 300 }}
          renderInput={(params) => <TextField {...params} label="Options" />}
        />
        <Button
          variant="outlined"
          onClick={() => handleAddNewResource(modalId, endpoint, statement)}
        >
          Create New
        </Button>
        <CreateNewFormModal id={modalId} />
      </Stack>
    </Box>
  );
};

export default AutoCompleteEditor;
