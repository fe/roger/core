// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { TextField } from "@mui/material";
import * as N3 from "n3";
import React from "react";

interface URIEditorProps {
  quad: N3.Quad;
  onChange: {
    handleChange: (value: string, quad: N3.Quad) => void;
  };
}

const URIEditor: React.FC<URIEditorProps> = ({ quad, onChange }) => {
  return (
    <TextField
      value={quad.object.value}
      onChange={(event) => {
        onChange.handleChange(event.target.value, quad);
      }}
      fullWidth
    />
  );
};

export default URIEditor;
