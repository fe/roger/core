// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  IconButton,
  Stack,
} from "@mui/material";
import React from "react";
import AddBoxIcon from "@mui/icons-material/AddBox";

/**
 * A component framing multiple field frames and the add button.
 */
export default function EditorFrame({
  children,
  data,
  helperText,
  label,
  onChange: { handleAdd },
  path,
  shape,
}) {
  return (
    <Card>
      <CardHeader title={label} subheader={helperText} />
      <CardContent
        sx={{
          paddingTop: 0,
          paddingBottom: 0,
        }}
      >
        <Stack spacing={2}>{children}</Stack>
      </CardContent>
      <CardActions>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            width: "100%",
            height: "auto",
          }}
        >
          <IconButton onClick={() => handleAdd(path, data, shape)}>
            <AddBoxIcon />
          </IconButton>
        </Box>
      </CardActions>
    </Card>
  );
}
