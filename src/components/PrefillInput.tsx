// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import SyncIcon from "@mui/icons-material/Sync";
import { IconButton, InputAdornment, TextField } from "@mui/material";
import * as N3 from "n3";
import React, { useMemo } from "react";

import IRIs from "../utils/IRIs";

interface PrefillInputProps {
  onSelectRemoteInstance: (input: string) => void;
  schema: N3.Store;
  selectedShape: {
    label: string;
    value: N3.Quad[];
  } | null;
}

const PrefillInput: React.FC<PrefillInputProps> = ({
  onSelectRemoteInstance: handleSelectRemoteInstance,
  schema,
  selectedShape,
}) => {
  const disabled = useMemo(() => {
    if (!selectedShape) {
      return true;
    }
    let subject = selectedShape.value[0]?.subject?.value;
    let data = schema.getQuads(subject, null, null, null);
    return !data?.some(
      (q) => q.predicate.value === IRIs.roger.prefillUrlTemplate
    );
  }, [selectedShape, schema]);

  /** The ID as typed in by the user. */
  const [userInput, setUserInput] = React.useState("");

  /** Set user input. */
  const handleChange = (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setUserInput(event.target.value);
  };

  /** Hand up ID on user pressing Enter. */
  const handleKeyPress = async (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === "Enter") {
      handleSelectRemoteInstance(userInput);
    }
  };

  /** Hand up ID on user clicking SyncIcon. */
  const handleClickSync = () => {
    handleSelectRemoteInstance(userInput);
  };

  return (
    <>
      {!disabled ? (
        <TextField
          label={"IRI"}
          helperText={undefined}
          value={userInput}
          onChange={handleChange}
          onKeyPress={handleKeyPress}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  disabled={disabled}
                  edge="end"
                  onClick={handleClickSync}
                  title="Sync"
                >
                  <SyncIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      ) : null}
    </>
  );
};

export default PrefillInput;
