// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

// hint: react-virtualized is dead... use react-window instead. OR: react-virtuoso?

import * as React from "react";
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
} from "@mui/material";
import Factory from "@rdfjs/data-model/Factory";

import type { DataFactory, Quad, Quad_Subject } from "@rdfjs/types";
import type { Store } from "n3";
import { SparqlConfig, updateResource } from "../../sparql/update";
import { getSubjects } from "../../sparql/getSubjects";
import { getBlankNodeQuads } from "../../sparql/getBlankNodeQuads";
import {
  getNodeShapes,
  getPropertyValue,
  getShape,
  getShapeProperties,
} from "../../utils";
import { Roger, SelectShape } from "../../index";

// workaround for a type-aware dataFactory
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
const dataFactory: DataFactory = new Factory();

const getPropertyShapes: Quad[][] = (schema: Store, nodeShape: Quad[]) => {
  const properties: Quad[] = getShapeProperties(schema, nodeShape);
  const propertyShapes: Quad[][] = [];
  for (const ps of properties) {
    propertyShapes.push(getShape(schema, ps.object.value));
  }
  return propertyShapes;
};

const needsEmbedding = (quad: Quad) =>
  quad.predicate.equals(
    dataFactory.namedNode("http://www.w3.org/ns/shacl#nodeKind")
  ) &&
  quad.object.equals(
    dataFactory.namedNode("http://www.w3.org/ns/shacl#BlankNode")
  );

const getDataFromQuads = (
  columnDatum: string[] | TableHeadData[],
  columnData: TableHeadData,
  subject: Quad_Subject,
  dataQuads: Quad[]
) => {
  const embeddedPathNode = dataFactory.namedNode(
    Object.keys(columnDatum).pop()
  );
  const pathNode = dataFactory.namedNode(Object.keys(columnData).pop());
  const elem: Quad = dataQuads.find(
    (quad: Quad) =>
      quad.subject.equals(subject) && quad.predicate.equals(pathNode)
  );

  if (elem && elem.object.termType === "BlankNode") {
    const blankNodeSubject: Quad_Subject = elem.object;
    const blankNodeQuads: Quad[] = dataQuads.filter((quad) =>
      quad.subject.equals(blankNodeSubject)
    );

    const valueQuad: Quad = blankNodeQuads.find((quad) =>
      quad.predicate.equals(embeddedPathNode)
    );
    return valueQuad ? valueQuad.object.value : "";
  } else {
    return elem ? elem.object.value : "";
  }
};

interface TableHeadData {
  [key: string]: string[] | TableHeadData[];
}

const assembleTableHeadData: Array<TableHeadData> = (
  schema: Store,
  propertyShapes: Quad[][]
) => {
  const tableHeadData: TableHeadData = [];
  for (const shape of propertyShapes) {
    if (shape.some((q: Quad) => needsEmbedding(q))) {
      const shapeName = shape.find(
        (property) =>
          property.predicate.value == "http://www.w3.org/ns/shacl#node"
      )?.object.value;
      const nodeShape: Quad[] = getShape(schema, shapeName);
      const newPropertyShapes: Quad[][] = getPropertyShapes(schema, nodeShape);
      const embedData = [];
      for (const newShape of newPropertyShapes) {
        embedData.push({
          [getPropertyValue(newShape, "http://www.w3.org/ns/shacl#path")]:
            getPropertyValue(newShape, "http://www.w3.org/ns/shacl#name"),
        });
      }
      tableHeadData.push({
        [getPropertyValue(shape, "http://www.w3.org/ns/shacl#path")]: embedData,
      });
    } else {
      tableHeadData.push({
        [getPropertyValue(shape, "http://www.w3.org/ns/shacl#path")]: [
          getPropertyValue(shape, "http://www.w3.org/ns/shacl#name"),
        ],
      });
    }
  }
  return tableHeadData;
};

interface TableFrameProps {
  schema: Store;
  config: SparqlConfig;
  defaultPerPage?: number;
}

// discuss: make embedding blank node properties configurable
export default function TableFrame({
  schema,
  config,
  defaultPerPage = 5,
}: Readonly<TableFrameProps>) {
  // states
  /** Zero-based page number. I.e. page 1 has index 0. */
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(defaultPerPage);
  const [selectedShape, setSelectedShape] = React.useState<Array<Quad>>([]);
  /** Couldn't this be derived from dataQuads? => No? Because it is used for number of results. */
  const [subjects, setSubjects] = React.useState<Array<Quad_Subject>>([]);
  /** The Quads of all table data currently displayed. */
  const [dataQuads, setDataQuads] = React.useState<Array<Quad>>([]);
  /** The currently selected TableRow's index. */
  const [selectedRow, setSelectedRow] = React.useState<number>(0);
  /** Track the "changed" state to prevent data loss on table item switch. */
  const [changed, setChanged] = React.useState<boolean>(false);
  /** Visibility of the save changes dialog. */
  // TODO: Used in commented out code below
  // const [saveChangesDialogOpen, setSaveChangesDialogOpen] = React.useState(false);

  // prepare
  const nodeShapes: Quad[][] = schema ? getNodeShapes(schema) : [];
  const propertyShapes: Quad[][] =
    schema && selectedShape.length
      ? getPropertyShapes(schema, selectedShape)
      : [];
  const tableHeadData: TableHeadData[] = assembleTableHeadData(
    schema,
    propertyShapes
  );

  // events
  const handleSelectShape = (shapeSubject: string) => {
    const shape: Quad[] = getShape(schema, shapeSubject);
    setSelectedShape(shape);
    setPage(0);
    setSelectedRow(1);
  };

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - subjects.length) : 0;

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage);
    setSelectedRow(1);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, defaultPerPage));
    // feat: recalculate the page of the currently selected row and set it
    setPage(0);
    setSelectedRow(1);
  };

  const handleClickTableRow = (
    event: React.MouseEvent<HTMLTableRowElement>
  ) => {
    if (changed) {
      alert("CHANGED");
    } else {
      setSelectedRow(event.currentTarget.rowIndex);
    }
  };

  // after a shape is selected, get ALL resource subjects for this shape's data
  React.useEffect(() => {
    if (selectedShape.length) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      (async () => {
        const targetClass: string = getPropertyValue(
          selectedShape,
          "http://www.w3.org/ns/shacl#targetClass"
        );
        const subjectsOfShape: Quad_Subject[] = await getSubjects(
          config.sparqlEndpoint,
          targetClass
        );
        setSubjects(subjectsOfShape);
      })();
    }
  }, [config.sparqlEndpoint, selectedShape]);

  // if either page, rowsPerPage, or subjects change, load data
  React.useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    (async () => {
      const currentPageSubjectsSlice = subjects.slice(
        page * rowsPerPage,
        page * rowsPerPage + rowsPerPage
      );
      const quadStore: Quad[] = [];
      for (const subject of currentPageSubjectsSlice) {
        const blankNodeQuads = await getBlankNodeQuads(
          config.sparqlEndpoint,
          subject.value
        );
        quadStore.push(...blankNodeQuads);
      }
      setDataQuads(quadStore);
    })();
  }, [config.sparqlEndpoint, page, rowsPerPage, subjects]);

  const dataIndex = selectedRow - 1;

  // select the subject of the first item of the current page
  const selectedSubject = subjects[page * rowsPerPage + dataIndex];
  // filter dataQuads down to the selected data subject (top-level quads only)
  const selectedSubjectData = dataQuads.filter((quad) =>
    quad.subject.equals(selectedSubject)
  );
  const selectedData: Quad[] = [];
  // now, add blank node data to the quads
  if (selectedSubjectData.length) {
    for (const quad of selectedSubjectData)
      if (quad.object.termType === "BlankNode") {
        const blankNodeSubject: Quad_Subject = quad.object;
        const blankNodeQuads: Quad[] = dataQuads.filter((quad) =>
          quad.subject.equals(blankNodeSubject)
        );
        selectedData.push(...blankNodeQuads);
      }
    selectedData.push(...selectedSubjectData);
  }

  async function handleSave(quads: Quad[]) {
    // error is currently handled in update.ts; but update should rather raise a custom error to be handled here.
    await updateResource(quads, config);
    setChanged(false);
  }

  return (
    <>
      <SelectShape
        onSelectShape={handleSelectShape}
        shapes={nodeShapes}
        selectedShape={selectedShape}
      />
      {selectedShape.length ? (
        <TableContainer component={Paper}>
          <Table
            sx={{ minWidth: 500 }}
            size="small"
            aria-label="custom pagination table"
          >
            <TableHead>
              <TableRow>
                <TableCell component="th" key="ID">
                  ID
                </TableCell>
                {tableHeadData.map((colData) =>
                  colData[Object.keys(colData).pop() ?? ""].map((colDatum) => (
                    <TableCell component="th" key={JSON.stringify(colDatum)}>
                      {typeof colDatum === "string"
                        ? colDatum
                        : colDatum[Object.keys(colDatum)]}
                    </TableCell>
                  ))
                )}
              </TableRow>
            </TableHead>

            <TableBody>
              {subjects
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((subject, index) => (
                  <TableRow
                    key={JSON.stringify(subject)}
                    onClick={handleClickTableRow}
                    selected={index === dataIndex}
                  >
                    <TableCell>{subject.value}</TableCell>
                    {tableHeadData.map((colData) =>
                      colData[Object.keys(colData).pop() ?? ""].map(
                        (colDatum) => (
                          <TableCell
                            component="td"
                            key={JSON.stringify([colDatum, subject])}
                          >
                            {getDataFromQuads(
                              colDatum,
                              colData,
                              subject,
                              dataQuads
                            )}
                          </TableCell>
                        )
                      )
                    )}
                  </TableRow>
                ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>

            <TableFooter>
              <TableRow>
                <TableCell align="left"></TableCell>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
                  colSpan={3}
                  count={subjects.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: {
                      "aria-label": "rows per page",
                    },
                    native: true,
                  }}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                  showFirstButton={true}
                  showLastButton={true}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      ) : (
        "Chose a Shape to load data."
      )}
      {selectedRow ? (
        <Roger
          schema={schema}
          data={selectedData?.length ? selectedData : undefined}
          onSave={config ? handleSave : undefined}
          useChanged={[changed, setChanged]}
        />
      ) : selectedShape.length ? (
        "Select Data to edit..."
      ) : (
        <></>
      )}
      {/* <SaveChangesDialog
        open={saveChangesDialogOpen}
        onDiscard={handleSaveChangesDialogDiscard}
        onCancel={handleSaveChangesDialogCancel}
        onSave={handleSaveChangesDialogSave}
        currentSubject={currentTabValue}
      /> */}
    </>
  );
}
