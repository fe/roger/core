// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2
import { Badge, Box, Button, ButtonGroup, Tab } from "@mui/material";
import { TabContext, TabList, TabPanel } from "@mui/lab";
import React from "react";
import dataFactory from "@rdfjs/data-model";
import { v4 as uuidv4 } from "uuid";

import type { Store } from "n3";
import * as N3 from "n3";

import type { Quad } from "@rdfjs/types";
import SaveChangesDialog from "../dialogs/SaveChangesDialog";
import SearchAndSelectDialog from "../dialogs/SearchAndSelectDialog";
import { Roger } from "../../index";
import { updateResource } from "../../sparql/update";

const getLabelFromIri = (iri: string) => {
  return iri.split("/").pop().split("#").pop();
};

interface TabbedFrameProps {
  readonly schema: Store;
}

export default function TabbedFrame({ schema }: TabbedFrameProps) {
  /** Visibility of the search result dialog. */
  const [open, setOpen] = React.useState(false);
  /** Visibility of the save changes dialog. */
  const [saveChangesDialogOpen, setSaveChangesDialogOpen] =
    React.useState(false);

  const [dataGraph, setDataGraph] = React.useState(new N3.Store());
  /** True if anything in the child component has changed. */
  const [changed, setChanged] = React.useState(false);

  const [currentTabValue, setCurrentTabValue] = React.useState("1");
  const [nextTabValue, setNextTabValue] = React.useState("");

  function handleChangeTab(_event: () => void, newValue: string) {
    if (changed) {
      setNextTabValue(newValue);
      setSaveChangesDialogOpen(true);
    } else setCurrentTabValue(newValue);
    // reset changed state on discard
    // setChanged(false);
  }

  /** Open dialog. */
  function handleClickOpenButton() {
    setOpen(true);
  }

  function handleSaveChangesDialogDiscard() {
    setSaveChangesDialogOpen(false);
    setChanged(false);
    setCurrentTabValue(nextTabValue);
  }

  async function handleSaveChangesDialogSave() {
    setSaveChangesDialogOpen(false);
    // handleSave
    const quads = dataGraph.getQuads(currentTabValue);
    await updateResource(quads);
    setChanged(false);
    setCurrentTabValue(nextTabValue);
  }

  function handleSaveChangesDialogCancel() {
    setSaveChangesDialogOpen(false);
  }

  // Feature request: create new mit ID-eingabe
  function createNew() {
    const nQuad: Quad = dataFactory.quad(
      dataFactory.namedNode(
        `https://www.sub.uni-goettingen.de/roger/schema#${uuidv4()}`
      ),
      dataFactory.namedNode("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
      dataFactory.blankNode()
    );
    const newDataGraph = new N3.Store(dataGraph.getQuads());
    newDataGraph.addQuad(nQuad);
    setDataGraph(newDataGraph);
    //change active tab to newly created item
    //setChanged(true);
  }

  return (
    <>
      <ButtonGroup>
        <Button onClick={createNew}>New</Button>
        <Button onClick={handleClickOpenButton}>Open</Button>
      </ButtonGroup>
      <SearchAndSelectDialog isOpen={[open, setOpen]} setData={setDataGraph} />
      {dataGraph.size ? (
        <Box sx={{ width: "100%", typography: "body1" }}>
          <TabContext value={currentTabValue}>
            <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
              <TabList
                onChange={handleChangeTab}
                aria-label="lab API tabs example"
                variant="scrollable"
                scrollButtons="auto"
              >
                <Tab label="Item One" value="1" />
                {dataGraph.getSubjects().map((subject) => (
                  <Tab
                    label={
                      <Badge
                        color="secondary"
                        variant="dot"
                        invisible={
                          !(currentTabValue === subject.value && changed)
                        }
                      >
                        {getLabelFromIri(subject.value)}
                      </Badge>
                    }
                    key={subject.value}
                    value={subject.value}
                  />
                ))}
              </TabList>
            </Box>
            <TabPanel value="1">
              {JSON.stringify(dataGraph.getSubjects())}
            </TabPanel>
            {dataGraph.getSubjects().map((subject) => (
              <TabPanel key={subject.value} value={subject.value}>
                <Roger
                  schema={schema}
                  data={dataGraph.getQuads(subject)}
                  useChanged={[changed, setChanged]}
                  onSave={updateResource}
                />
              </TabPanel>
            ))}
          </TabContext>
        </Box>
      ) : (
        ""
      )}
      <SaveChangesDialog
        open={saveChangesDialogOpen}
        onDiscard={handleSaveChangesDialogDiscard}
        onCancel={handleSaveChangesDialogCancel}
        onSave={handleSaveChangesDialogSave}
        currentSubject={currentTabValue}
      />
    </>
  );
}
