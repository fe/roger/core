// SPDX-FileCopyrightText: 2022, 2023, 2024 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import * as React from "react";
import Stack from "@mui/material/Stack";
import ButtonGroup from "@mui/material/ButtonGroup";
import Button from "@mui/material/Button";
import type { Quad } from "@rdfjs/types";
import type { Store } from "n3";
import SearchAndSelectDialog from "../dialogs/SearchAndSelectDialog";
import NewIdDialog from "../dialogs/NewIdDialog";

import type { SparqlConfig } from "../../sparql/update";
import { updateResource } from "../../sparql/update";
import { Roger } from "../../index";

interface SingleFrameProps {
  readonly schema: Store;
  readonly idNamespace?: string;
  readonly config?: SparqlConfig;
}

export default function SingleFrame({
  schema: schemaStore,
  idNamespace = "http://example.org/",
  config,
}: SingleFrameProps) {
  /** Visibility of the search result dialog. */
  const [searchAndSelectDialogOpen, setSearchAndSelectDialogOpen] =
    React.useState<boolean>(false);
  /** Visibility of the new ID dialog. */
  const [newIdDialogOpen, setNewIdDialogOpen] = React.useState<boolean>(false);
  /** An array of Quads to store data retrieved from SPARQL endpoint. */
  const [dataQuads, setDataQuads] = React.useState<Array<Quad>>([]);
  /** Track the "changed" state to prevent data loss on "New" or "Open". */
  const [changed, setChanged] = React.useState<boolean>(false);

  /** Open dialog. */
  function handleClickOpenButton() {
    setSearchAndSelectDialogOpen(true);
  }

  function handleClickNewButton() {
    setNewIdDialogOpen(true);
  }

  async function handleSave(quads: Quad[]) {
    await updateResource(quads, config);
  }

  return (
    <Stack direction="row" spacing={2}>
      <ButtonGroup orientation="vertical">
        <Button onClick={handleClickNewButton}>New</Button>
        <Button onClick={handleClickOpenButton}>Open</Button>
      </ButtonGroup>
      <NewIdDialog
        isOpen={[newIdDialogOpen, setNewIdDialogOpen]}
        ns={idNamespace}
        schema={schemaStore}
        onCreate={[dataQuads, setDataQuads]}
      />
      <SearchAndSelectDialog
        isOpen={[searchAndSelectDialogOpen, setSearchAndSelectDialogOpen]}
        setData={setDataQuads}
        single
      />

      <Roger
        schema={schemaStore}
        data={dataQuads}
        onSave={config ? handleSave : undefined}
        useChanged={[changed, setChanged]}
      />
    </Stack>
  );
}
