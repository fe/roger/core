// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

import babel from "@rollup/plugin-babel";
import commonjs from "@rollup/plugin-commonjs";
import resolve from "@rollup/plugin-node-resolve";
import css from "rollup-plugin-css-only";
import del from "rollup-plugin-delete";
import external from "rollup-plugin-peer-deps-external";

import pkg from "./package.json";

const config = {
  input: pkg.source,
  output: [
    { file: pkg.main, format: "cjs" },
    { file: pkg.module, format: "esm" },
  ],
  plugins: [
    del({ targets: ["dist/*"] }),
    external(),
    resolve({
      extensions: [".js", ".jsx", ".ts", ".tsx"],
    }),
    commonjs(),
    babel({
      babelHelpers: "bundled",
      exclude: ["**/node_modules/**", "preview/**"],
      extensions: [".js", ".jsx", ".ts", ".tsx"],
    }),
    css({
      output: "dist/styles.css",
    }),
  ],
  external: Object.keys(pkg.peerDependencies || {}),
};

export default config;
