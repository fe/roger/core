// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

import React, { useEffect } from "react";
import ReactDOM from "react-dom/client";

import { parsePromise, Roger } from "@fe/roger-core";
import * as N3 from "n3";
//import { v4 as uuidv4 } from "uuid";
import { rosendata, rosenEndpoints } from "./data/rosendata";
import { rosenschema } from "./data/rosenschema";

const haSchema = `
@prefix dash: <http://datashapes.org/dash#>.
@prefix hadata: <http://hannah-arendt-edition.net/elements/>.
@prefix hashapes: <http://hannah-arendt-edition.net/shapes/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix roger: <https://www.sub.uni-goettingen.de/roger/schema#>.
@prefix sh: <http://www.w3.org/ns/shacl#>.
@prefix skos: <http://www.w3.org/2004/02/skos/core#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.


hashapes:ItemShape
  a sh:NodeShape ;
  sh:name "Commentary Item" ;
  sh:targetClass hadata:Item ;
  sh:property hashapes:noteShape ;
  sh:property hashapes:volumeShape .

hashapes:NoteShape
  a sh:NodeShape ;
  #roger:formNode false ;
  sh:name "Note" ;
  sh:targetClass hadata:Note ;
  sh:property hashapes:commentShape ;
  sh:property hashapes:languageShape ;
  sh:property hashapes:renderShape ;
  sh:property hashapes:bibRefShape ;
  sh:property hashapes:internalNoteShape .


hashapes:volumeShape
  a sh:PropertyShape ;
  sh:path hadata:listCorresp ;
  sh:nodeKind sh:IRI ;
  dash:editor dash:TextFieldEditor ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Volume" ;
  sh:description "The corresponding volume." ;
  sh:order "1"^^xsd:decimal .

hashapes:noteShape
  a sh:PropertyShape ;
  sh:path hadata:hasNote ;
  sh:class hadata:Note ;
  sh:node hashapes:NoteShape ;
  sh:nodeKind sh:BlankNode ;
  sh:minCount 1;
  sh:name "Note" ;
  sh:description "An editorial Note." ;
  dash:editor dash:DetailsEditor ;
  sh:order "2"^^xsd:decimal .

hashapes:commentShape
  a sh:PropertyShape ;
  sh:path hadata:p ;
  sh:datatype xsd:string ;
  dash:editor roger:HannahArendtRichTextEditor ;
  sh:name "Editorial comment" ;
  sh:description "Editorial comment text." ;
  sh:order "1"^^xsd:decimal .

hashapes:languageShape
  a sh:PropertyShape ;
  sh:path hadata:lang ;
  sh:datatype xsd:string ;
  sh:in ("en" "de" "fr" "la" "grc") ;  # should be entities that match an iso language code to a label; now they are BlankNodes
  dash:editor dash:EnumSelectEditor ;
  sh:name "Comment language" ;
  sh:description "Select the language of the comment." ;
  sh:order "2"^^xsd:decimal .


hashapes:renderShape
  a sh:PropertyShape ;
  sh:path hadata:rend ;
  sh:datatype xsd:string ;
  sh:in ("default" "yes" "no") ; # also become blank nodes...
  dash:editor dash:EnumSelectEditor ;
  sh:name "Render" ;
  sh:description "Rendering mode." ;
  sh:order "3"^^xsd:decimal .

hashapes:bibRefShape
  a sh:PropertyShape ;
  sh:path hadata:biblCorresp ;
  #sh:nodeKind sh:IRIOrLiteral ;
  sh:name "Bibl. Signature" ;
  sh:description "" ;
  dash:editor dash:TextFieldEditor ;
  sh:order "4"^^xsd:decimal .

hashapes:bibRefPagesShape
  a sh:PropertyShape ;
  sh:path hadata:biblScope ;
  #sh:nodeKind sh:IRIOrLiteral ;
  sh:name "Pagenumber" ;
  sh:description "." ;
  dash:editor dash:TextFieldEditor ;
  sh:order "4"^^xsd:decimal .

hashapes:bibRefFromPageShape
  a sh:PropertyShape ;
  sh:path hadata:biblScopeFrom ;
  #sh:nodeKind sh:IRIOrLiteral ;
  sh:name "page series from" ;
  sh:description "" ;
  dash:editor dash:TextFieldEditor ;
  sh:order "4"^^xsd:decimal .

hashapes:bibRefToPageShape
  a sh:PropertyShape ;
  sh:path hadata:biblScopeTo ;
  #sh:nodeKind sh:IRIOrLiteral ;
  sh:name "page series to" ;
  sh:description "" ;
  dash:editor dash:TextFieldEditor ;
  sh:order "4"^^xsd:decimal .

hashapes:bibRefAdditionalShape
  a sh:PropertyShape ;
  sh:path hadata:biblScopeN ;
  #sh:nodeKind sh:IRIOrLiteral ;
  sh:name "Short-Reference (addition)" ;
  sh:description "" ;
  dash:editor dash:TextFieldEditor ;
  sh:order "4"^^xsd:decimal .

  hashapes:bibRefAloneShape
  a sh:PropertyShape ;
  sh:path hadata:bibl ;
  #sh:nodeKind sh:IRIOrLiteral ;
  sh:name "Short-Reference (alone)" ;
  sh:description "" ;
  dash:editor dash:TextFieldEditor ;
  sh:order "4"^^xsd:decimal .

hashapes:internalNoteShape
  a sh:PropertyShape ;
  sh:path hadata:internalNote ;
  sh:datatype xsd:string ;
  dash:editor dash:TextAreaEditor ;  # hashapes:RichTextEditor
  dash:singleLine false ;
  sh:name "Internal Notes" ;
  sh:maxCount 1 ;
  sh:order "99"^^xsd:decimal .
`;

const bdnSchema = `@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix dc: <http://purl.org/dc/elements/1.1/>.
@prefix textgrid: <http://textgridrep.de/textgrid:>.
@prefix roger: <https://www.sub.uni-goettingen.de/roger/schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix sh: <http://www.w3.org/ns/shacl#>.
@prefix dash: <http://datashapes.org/dash#>.
@prefix bdn: <https://bdn-edition.de/terms/>.
@prefix bdnshapes: <https://bdn-edition.de/shapes/>.
@prefix gndo: <https://d-nb.info/standards/elementset/gnd#>.
@prefix owl: <http://www.w3.org/2002/07/owl#>.
#deprecated @prefix bf: <http://bibframe.org/vocab/>.
@prefix bf: <http://id.loc.gov/ontologies/bibframe/>.
@prefix rdagr1: <http://rdvocab.info/Elements/>.
@prefix bibo: <http://purl.org/ontology/bibo/>.
@prefix rdau: <http://rdaregistry.info/Elements/u/>.

# NodeShapes

bdnshapes:PersonShape
    a sh:NodeShape ;
    rdfs:label "Person Shape" ;
    sh:name "Person" ;
    sh:targetClass bdn:Person ;
    roger:formNode true ;
    roger:exportFileNameProperty bdnshapes:preferredNameForThePersonShape; # must be a field that allows a single value only, currently a workaround in the implementation prevents failure in other cases
    roger:prefillUrlTemplate "/gnd/{}/about/lds" ;
    sh:property bdnshapes:commentShape ,
                bdnshapes:preferredNameForThePersonShape ,
                bdnshapes:variantNameForThePersonShape ,
                bdnshapes:professionOrOccupationShape ,
                bdnshapes:biographicalOrHistoricalInformationShape ,
                bdnshapes:dateOfBirthShape ,
                bdnshapes:dateOfDeathShape ,
                bdnshapes:placeOfActivityShape,
                bdnshapes:sameAsShape .

bdnshapes:InformationCarrierShape
    a sh:NodeShape ;
    rdfs:label "Informationsträger Shape" ;
    sh:targetClass bdn:InformationCarrier ;
    roger:formNode false ;
    sh:property bdnshapes:commentShape ,
                bdnshapes:titleShape ,
                bdnshapes:alternativeShape ,
                bdnshapes:isItemOfShape ,
                bdnshapes:sameAsShape .

bdnshapes:WorkShape
    a sh:NodeShape ;
    rdfs:label "Werk Shape" ;
    sh:targetClass bdn:Work ;
    roger:formNode true ;
    roger:exportFileNameProperty bdnshapes:titleShape;
    roger:prefillUrlTemplate "/b3k/{}?output=ttl" ;
    sh:property bdnshapes:commentShape ,
                bdnshapes:titleShape ,
                bdnshapes:alternativeShape ,
                bdnshapes:creatorShape ,
                bdnshapes:sameAsShape .

bdnshapes:ArticleShape
    a sh:NodeShape ;
    rdfs:label "Artikel Shape" ;
    sh:targetClass bdn:Article ;
    roger:formNode true ;
    roger:exportFileNameProperty bdnshapes:titleShape;
    roger:prefillUrlTemplate "/b3k/{}?output=ttl" ;
    sh:property bdnshapes:commentShape ,
                bdnshapes:titleShape ,
                bdnshapes:alternativeShape ,
                bdnshapes:publisherShape ,
                bdnshapes:instantiatesShape ,
                bdnshapes:providerStatementShape ,
                bdnshapes:issuedShape ,
                bdnshapes:extentShape ,
                bdnshapes:placeOfPublicationShape ,
                bdnshapes:p60083Shape ,
                bdnshapes:isPartOfShape ,
                bdnshapes:pagesShape ,
                bdnshapes:sameAsShape .

bdnshapes:BookShape
    a sh:NodeShape ;
    rdfs:label "Buch Shape" ;
    sh:targetClass bdn:Book ;
    roger:formNode true ;
    roger:exportFileNameProperty bdnshapes:titleShape;
    roger:prefillUrlTemplate "/b3k/{}?output=ttl" ;
    sh:property bdnshapes:commentShape ,
                bdnshapes:titleShape ,
                bdnshapes:alternativeShape ,
                bdnshapes:publisherShape ,
                bdnshapes:instantiatesShape ,
                bdnshapes:providerStatementShape ,
                bdnshapes:issuedShape ,
                bdnshapes:extentShape ,
                bdnshapes:placeOfPublicationShape ,
                bdnshapes:p60083Shape ,
                bdnshapes:editionShape ,
                bdnshapes:sameAsShape .

# PropertyShapes

bdnshapes:commentShape
    a sh:PropertyShape ;
    sh:path rdfs:comment ;
    sh:datatype xsd:string ;
    dash:editor dash:TextAreaEditor ;
    dash:singleLine false ;
    sh:name "Kommentar" ;
    sh:maxCount 1 ;
    sh:order "0"^^xsd:decimal .

bdnshapes:preferredNameForThePersonShape
    a sh:PropertyShape ;
    sh:path gndo:preferredNameForThePerson ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Name" ;
    sh:minCount 1 ;
    sh:maxCount 1 ;
    sh:description "Der bevorzugte Name der Person." ;
    sh:order "1"^^xsd:decimal .

bdnshapes:variantNameForThePersonShape
    a sh:PropertyShape ;
    sh:path gndo:variantNameForThePerson ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Namensvariante" ;
    sh:description "Ein alternativer Name für die Person." ;
    sh:order "2"^^xsd:decimal .

bdnshapes:professionOrOccupationShape
    a sh:PropertyShape ;
    sh:path gndo:professionOrOccupationAsLiteral ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Tätigkeitsfeld" ;
    sh:description "Eine Tätigkeit oder ein Tätigkeitsfeld der Person." ;
    sh:order "3"^^xsd:decimal .

bdnshapes:biographicalOrHistoricalInformationShape
    a sh:PropertyShape ;
    sh:path gndo:biographicalOrHistoricalInformation ;
    sh:datatype xsd:string ;
    dash:editor dash:TextAreaEditor ;
    dash:singleLine false ;
    sh:name "Biographische oder historische Information" ;
    sh:order "4"^^xsd:decimal .

bdnshapes:dateOfBirthShape
    a sh:PropertyShape ;
    sh:path gndo:dateOfBirth ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Geburtsdatum" ;
    sh:maxCount 1 ;
    sh:description "Das Geburtsdatum der Person." ;
    sh:order "5"^^xsd:decimal .

bdnshapes:dateOfDeathShape
    a sh:PropertyShape ;
    sh:path gndo:dateOfDeath ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Sterbedatum" ;
    sh:maxCount 1 ;
    sh:description "Das Sterbedatum der Person." ;
    sh:order "6"^^xsd:decimal .

bdnshapes:placeOfActivityShape
    a sh:PropertyShape ;
    sh:path gndo:placeOfActivity ;
    sh:nodeKind sh:IRI ;
    dash:editor dash:URIEditor ;
    sh:name "Wirkungsort" ;
    sh:description "Eine Wikungsstätte, die mit der Person verbunden ist." ;
    sh:order "7"^^xsd:decimal .

bdnshapes:sameAsShape
    a sh:PropertyShape ;
    sh:path owl:sameAs ;
    sh:nodeKind sh:IRI ;
    dash:editor dash:URIEditor ;
    sh:name "Verknüpfter Datensatz" ;
    sh:order "15"^^xsd:decimal .

bdnshapes:titleShape
    a sh:PropertyShape ;
    sh:path dc:title ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Titel" ;
    sh:minCount 1 ;
    sh:maxCount 1 ;
    sh:description "Die bevorzugte Bezeichnung." ;
    sh:order "1"^^xsd:decimal .

bdnshapes:alternativeShape
    a sh:PropertyShape ;
    sh:path dcterms:alternative ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Alternativer Titel" ;
    sh:description "Eine alternative Bezeichnung." ;
    sh:order "2"^^xsd:decimal .

bdnshapes:isItemOfShape
    a sh:PropertyShape ;
    sh:path bdn:isItemOf ;
    sh:class bdn:Instance ;
    sh:node bdnshapes:ArticleShape ;
    sh:node bdnshapes:BookShape ;
    dash:editor dash:AutoCompleteEditor ;
    sh:nodeKind sh:IRI ;
    sh:maxCount 1 ;
    sh:name "Verknüpfte Werkinstanz" ;
    sh:description "Verknüpfung zu einer Ausgabe eines Werks" ;
    sh:order "3"^^xsd:decimal .

bdnshapes:creatorShape
    a sh:PropertyShape ;
    sh:path dcterms:creator ;
    sh:class bdn:Person ;
    sh:node bdnshapes:PersonShape ;
    dash:editor dash:AutoCompleteEditor ;
    sh:nodeKind sh:IRI ;
    sh:maxCount 1 ;
    sh:name "Autor" ;
    sh:description "Ein Autor des Werks." ;
    sh:order "3"^^xsd:decimal .

bdnshapes:publisherShape
    a sh:PropertyShape ;
  # path and class?
    sh:path dcterms:publisher ;
    sh:class bdn:Person ;
    sh:node bdnshapes:PersonShape ;
    dash:editor dash:AutoCompleteEditor ;
    sh:nodeKind sh:IRI ;
    sh:name "Herausgeber" ;
    sh:description "Ein Herausgeber der Ausgabe." ;
    sh:order "10"^^xsd:decimal .

bdnshapes:instantiatesShape
    a sh:PropertyShape ;
    sh:path bdn:instantiates ;
    sh:class bdn:Work ;
    sh:node bdnshapes:WorkShape ;
    dash:editor dash:AutoCompleteEditor ;
    sh:nodeKind sh:IRI ;
    sh:maxCount 1 ;
    sh:name "Verknüpftes Werk" ;
    sh:description "Das zu einer Ausgabe zugehörige Werk." ;
    sh:order "11"^^xsd:decimal .

bdnshapes:providerStatementShape
    a sh:PropertyShape ;
    sh:path rdagr1:publicationStatement ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Erscheinungsvermerk" ;
    sh:maxCount 1 ;
    sh:description "Ein ausgabenspezifischer Erscheinungvermerk" ;
    sh:order "4"^^xsd:decimal .

bdnshapes:issuedShape
    a sh:PropertyShape ;
    sh:path dcterms:issued ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Veröffentlichungsdatum" ;
    sh:maxCount 1 ;
    sh:description "Veröffentlichungsdatum..." ;
    sh:order "5"^^xsd:decimal .

bdnshapes:extentShape
    a sh:PropertyShape ;
    sh:path dcterms:extent ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Kollationsvermerk" ;
    sh:maxCount 1 ;
    sh:description "Vermerk über den Umfang einer Ausgabe." ;
    sh:order "6"^^xsd:decimal .

bdnshapes:editionShape
    a sh:PropertyShape ;
    sh:path bibo:edition ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Edition" ;
    sh:maxCount 1 ;
    sh:description "Edition." ;
    sh:order "8"^^xsd:decimal .

bdnshapes:placeOfPublicationShape
    a sh:PropertyShape ;
    sh:path rdagr1:placeOfPublication ;
    sh:nodeKind sh:IRI ;
    dash:editor dash:TextFieldEditor ;
    sh:name "Erscheinungsort" ;
    sh:maxCount 1 ;
    sh:description "Angabe zum Erscheinungsort einer Ausgabe." ;
    sh:order "7"^^xsd:decimal .

bdnshapes:p60083Shape
    a sh:PropertyShape ;
    sh:path rdau:p60083 ;
    sh:nodeKind sh:IRI ;
    dash:editor dash:URIEditor ;
    sh:name "Digitalisat" ;
    sh:description "Link auf ein Digitalisat der entsprechenden Ausgabe." ;
    sh:order "9"^^xsd:decimal .

bdnshapes:pagesShape
    a sh:PropertyShape ;
    sh:path bibo:pages ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Seiten" ;
    sh:maxCount 1 ;
    sh:description "Seitenbereich des Artikels." ;
    sh:order "12"^^xsd:decimal .

bdnshapes:isPartOfShape
    a sh:PropertyShape ;
    sh:path bdn:isPartOf ;
    sh:class bdn:Book ;
    sh:node bdnshapes:BookShape ;
    dash:editor dash:AutoCompleteEditor ;
    sh:nodeKind sh:IRI ;
    sh:maxCount 1 ;
    sh:name "Bestandteil von" ;
    sh:description "Verknüpfung mit einer Publikation (z.B. Zeitschrift, Sammelband o.Ä.), in der der Artikel veröffentlicht wurde." ;
    sh:order "13"^^xsd:decimal .`;

const bdnPersonXml = `<rdf:RDF
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
xmlns:owl="http://www.w3.org/2002/07/owl#"
xmlns:bdn="https://bdn-edition.de/bdn#"
xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
  <bdn:Person rdf:about="http://textgridrep.de/textgrid:255k8">
    <gndo:preferredNameForThePerson>Abū-Tammām Ḥabīb Ibn-Aus aṭ-Ṭāʾī</gndo:preferredNameForThePerson>
    <gndo:variantNameForThePerson>Abu</gndo:variantNameForThePerson>
    <gndo:variantNameForThePerson>Abi Temmam</gndo:variantNameForThePerson>
    <gndo:variantNameForThePerson>Abū-Tammām Ḥabīb Ibn-Aus aṭ-Ṭāʾī Abū-Tammām Ḥabīb Ibn-Aus aṭ-Ṭāʾī:</gndo:variantNameForThePerson>
    <owl:sameAs rdf:resource="http://d-nb.info/gnd/118859331"/>
    <gndo:dateOfDeath rdf:datatype="xsd:date">846</gndo:dateOfDeath>
    <gndo:dateOfBirth rdf:datatype="xsd:date">806</gndo:dateOfBirth>
    <gndo:professionOrOccupation>Schriftsteller</gndo:professionOrOccupation>
    <gndo:biographicalOrHistoricalInformation>Arab. Dichter</gndo:biographicalOrHistoricalInformation>
    <gndo:biographicalOrHistoricalInformation>Schriftsteller</gndo:biographicalOrHistoricalInformation>
    <gndo:biographicalOrHistoricalInformation>Lebensdaten: 806-846</gndo:biographicalOrHistoricalInformation>
    <rdfs:comment/>
  </bdn:Person>
</rdf:RDF>`;

const bdnBookXml = `<rdf:RDF
xmlns:rdagr1="http://rdvocab.info/Elements/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:subforms="http://www.sub.uni-goettingen.de/subformterms#"
xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
xmlns:bdnshapes="https://bdn-edition.de/bdnshapes#"
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:bf="http://bibframe.org/vocab/"
xmlns:tgforms="http://www.tgforms.de/terms#"
xmlns:owl="http://www.w3.org/2002/07/owl#"
xmlns:bdn="https://bdn-edition.de/bdn#"
xmlns:gndo="https://d-nb.info/standards/elementset/gndo#"
xmlns:bibo="http://purl.org/ontology/bibo/"
xmlns:rdau="http://rdaregistry.info/Elements/u/"
xmlns:sh="http://www.w3.org/ns/shacl#"
xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
  <bdn:Book rdf:about="http://textgridrep.de/textgrid:2506v">
    <owl:sameAs rdf:resource="http://swb.bsz-bw.de/DB=2.1/PPNSET?PPN=021642664"/>
    <dcterms:extent>[8] Bl., 256 S. ; 8°</dcterms:extent>
    <dcterms:title>Ueber die Nutzbarkeit des Predigtamtes und deren Beförderung</dcterms:title>
    <dcterms:issued>1773</dcterms:issued>
    <bf:providerStatement>Berlin : Voß 1773</bf:providerStatement>
    <bdn:instantiates rdf:resource="http://textgridrep.de/textgrid:2506t"/>
    <bf:edition>Zweyte, vermehrte Auflage</bf:edition>

  <dcterms:publisher/><rdfs:comment/></bdn:Book>
</rdf:RDF>`;

const bdnBookTtl = `@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix dc: <http://purl.org/dc/terms/> .
@prefix ns0: <http://bibframe.org/vocab/> .
@prefix ns1: <https://bdn-edition.de/bdn#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .

<http://textgridrep.de/textgrid:2506v>
  a <https://bdn-edition.de/bdn#Book> ;
  owl:sameAs <http://swb.bsz-bw.de/DB=2.1/PPNSET?PPN=021642664> ;
  dc:extent "[8] Bl., 256 S. ; 8°" ;
  dc:title "Ueber die Nutzbarkeit des Predigtamtes und deren Beförderung" ;
  dc:issued "1773" ;
  ns0:providerStatement "Berlin : Voß 1773" ;
  ns1:instantiates <http://textgridrep.de/textgrid:2506t> ;
  ns0:edition "Zweyte, vermehrte Auflage" ;
  dc:publisher "" ;
  rdfs:comment "argl!"@de .`;

const bdnTtlData = `@prefix ns0: <https://d-nb.info/standards/elementset/gnd#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .

<http://textgridrep.de/textgrid:255k8>
  a <https://bdn-edition.de/bdn#Person> ;
  ns0:preferredNameForThePerson "Abū-Tammām Ḥabīb Ibn-Aus aṭ-Ṭāʾī" ;
  ns0:variantNameForThePerson "Abu / Abi Temmam / Abū-Tammām Ḥabīb Ibn-Aus aṭ-Ṭāʾī Abū-Tammām Ḥabīb Ibn-Aus aṭ-Ṭāʾī:", "Abu", "Abi Temmam", "Abū-Tammām Ḥabīb Ibn-Aus aṭ-Ṭāʾī Abū-Tammām Ḥabīb Ibn-Aus aṭ-Ṭāʾī:" ;
  ns0:variantNameForThePerson "Apu" ;
  owl:sameAs <http://d-nb.info/gnd/118859331> ;
  ns0:dateOfDeath "846"^^xsd:date ;
  ns0:dateOfBirth "806"^^xsd:date ;
  ns0:professionOrOccupation "Schriftsteller" ;
  ns0:biographicalOrHistoricalInformation "Arab. Dichter", "Schriftsteller", "Lebensdaten: 806-846" ;
  rdfs:comment "" .`;

const haData = `@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix ha: <http://hannah-arendt-edition.net/elements/>.

<http://hannah-arendt-edition.net/comments/lmw2q729>
    a ha:Item;
    ha:listId "XIV-commentaryDataLifeOfTheMind" ;
    ha:listCorresp <http://hannah-arendt-edition.net/volume/XIV> ;
    ha:hasNote [
        a ha:Note ;
        ha:internalNote "“Will to be pleased, you with yourself” (thelēson aresai autos seautō), and Epictetus adds: “Will to appear noble to the god” (thelēson kalos phanenai tō theō) (61)" ;
        ha:lang "grc" ;
        ha:rend "yes" ;
        ha:p """<p xmlns="http://www.tei-c.org/ns/1.0"><foreign xml:lang="en">“</foreign>Πῶς οὖν γένηται τοῦτο; θέλησον <hi rend="underline">ἀρέσαι αὐτος</hi> ποτε <hi rend="underline">σεαυτῷ</hi>, θέλησον καλὸς φανῆναι τῷ θεῷ·<foreign xml:lang="en">”</foreign><foreign xml:lang="en">In margin: line.</foreign></p>""" ;
        ha:biblCorresp <http://hannah-arendt-edition.net/bibliography#EpictetDiatribaiHA> ;
        ha:biblScope "167" ;
        ha:biblScopeN "2.18.19" ;
    ] ;
    ha:hasNote [
        a ha:Note ;
        ha:rend "yes" ;
        ha:p """<p xmlns="http://www.tei-c.org/ns/1.0">“How, then, is this to be done? Make up your mind at last to please <hi rend="underline">your true self</hi>, make up your mind to appear noble to God; [...].” In margin: “Will–<foreign xml:lang="grc">θελησον</foreign>” ↑Make up your; “<foreign xml:lang="grc">θελησον αρέσαι αὐτὸς σεαυτῷ</foreign>” ↑Make up your mind at last to please your true self; “<foreign xml:lang="grc">θελησον</foreign>.” ↑make up your mind.</p>""" ;
        ha:biblCorresp <http://hannah-arendt-edition.net/bibliography#EpictetDiscoursesOatesHA> ;
        ha:bibl "Genesis 1:31 (RSV)" ;
        ha:biblScopeFrom "123" ;
        ha:biblScopeTo "234" ;
        ha:biblScopeN "2.18" ;
    ] .
`;

const parse = async (input) => {
  const store = new N3.Store();
  const parser = new N3.Parser();
  return await parsePromise(parser, input, store);
};

function App({ data, schema, endpoints }) {
  const [dataStore, setDataStore] = React.useState();
  useEffect(() => {
    (async () => {
      let result;
      try {
        result = await parse(data);
      } catch (error) {
        console.log(error.message);
      }
      if (result) setDataStore(result);
    })();
  }, [data]);

  const [schemaStore, setSchemaStore] = React.useState();
  useEffect(() => {
    (async () => {
      let result;
      try {
        result = await parse(schema);
      } catch (error) {
        console.log(error.message);
      }
      if (result) setSchemaStore(result);
    })();
  }, [schema]);

  return <Roger schema={schema} />;
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App data={bdnTtlData} schema={bdnSchema} endpoints={rosenEndpoints} />
  </React.StrictMode>
);
