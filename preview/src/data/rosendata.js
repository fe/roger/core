// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { v4 as uuidv4 } from "uuid";

const created = new Date().toISOString();
const uniqueId = uuidv4();
const literarySource = uuidv4();
const literarySource_passage = uuidv4();
const notedIn = uuidv4();
const egotext = uuidv4();
const instace = uuidv4();
const instance_passage = uuidv4();
const place = uuidv4();
const publisher = uuidv4();
const work = uuidv4();

export const rosendata = `
<https://www.sub.uni-goettingen.de/roger/schema#9c052e77-faea-4296-9cd5-44c85e14f84f> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/ns/oa#Annotation> .
<https://www.sub.uni-goettingen.de/roger/schema#9c052e77-faea-4296-9cd5-44c85e14f84f> <http://www.w3.org/ns/oa#hasBody> _:genid2db3857b75623a4e1099b0a4b432a96ebf1132db1 .
_:genid2db3857b75623a4e1099b0a4b432a96ebf1132db1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/lso/intro/beta202304#INT7_Interpretament> .
_:genid2db3857b75623a4e1099b0a4b432a96ebf1132db1 <https://w3id.org/lso/intro/beta202304#R21_identifies> _:genid2db3857b75623a4e1099b0a4b432a96ebf1132db2 .
_:genid2db3857b75623a4e1099b0a4b432a96ebf1132db1 <http://www.example.org/met/ontologies/2023/0/RosenzweigAnnotation#literarySource> <http://www.example.org/met/ontologies/2023/0/RosenzweigAnnotation#passage_modal-03676a09-b530-4e4a-a291-c9519219d46e> .
_:genid2db3857b75623a4e1099b0a4b432a96ebf1132db2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/lso/intro/beta202304#INT3_IntertextualRelationship> .
_:genid2db3857b75623a4e1099b0a4b432a96ebf1132db2 <http://www.example.org/met/ontologies/2023/0/RosenzweigAnnotation#hasFormalAppearance> _:genid2db3857b75623a4e1099b0a4b432a96ebf1132db3 .
_:genid2db3857b75623a4e1099b0a4b432a96ebf1132db3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.example.org/met/ontologies/2023/0/RosenzweigAnnotation#FormalAppearance> .
_:genid2db3857b75623a4e1099b0a4b432a96ebf1132db3 <http://www.example.org/met/ontologies/2023/0/RosenzweigAnnotation#hasQuotationMarks> "false" .
_:genid2db3857b75623a4e1099b0a4b432a96ebf1132db3 <http://www.example.org/met/ontologies/2023/0/RosenzweigAnnotation#titleStated> "true" .
_:genid2db3857b75623a4e1099b0a4b432a96ebf1132db3 <http://www.example.org/met/ontologies/2023/0/RosenzweigAnnotation#authorStated> "false" .

`;

export const rosenEndpoints = {
  endpoint: "http://localhost:8080/rdf4j-server/repositories/star",
  update: "http://localhost:8080/rdf4j-server/repositories/star/statements",
};
